package com.ismael.fisiopia.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.icu.lang.UCharacterEnums;
import android.media.tv.TvInputService;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.games.GamesMetadata;
import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacDetailPresenter;
import com.ismael.fisiopia.presenter.TratamientoPresenterImpl;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.util.DrawerUtil;
import com.ismael.fisiopia.util.MyArrayAdapter;
import com.ismael.fisiopia.util.SessionManager;

import java.util.List;

public class TratamientosActivity extends AppCompatActivity {

    TratamientoPresenterImpl tratamientoPresenterImpl;
    ListView tratamientoView;
    ProgressBar progressBar;
    RelativeLayout layout;
    FloatingActionButton btnAddTrat;
    private SessionManager sessionManager;
    private boolean esPaciente;
    String usernameSelected = new String();
    TextView emptyTratamiento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Treatments view - START ");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tratamiento_activity);

        tratamientoPresenterImpl = new TratamientoPresenterImpl(this);
        tratamientoView = (ListView) findViewById(R.id.tratamientoview);
        progressBar = (ProgressBar) findViewById(R.id.progressBarTratamientos);
        layout = (RelativeLayout) findViewById(R.id.progressLayoutTrat);
        this.sessionManager = new SessionManager(this);
        btnAddTrat = (FloatingActionButton) findViewById(R.id.btnAddTrat);
        esPaciente = sessionManager.getEsPaciente();
        emptyTratamiento = (TextView) findViewById(R.id.txtEmptyTratamiento);
        setTitle(R.string.title_treatment);


        if (esPaciente) {
            btnAddTrat.setVisibility(View.GONE);
        } else {
            btnAddTrat.setVisibility(View.VISIBLE);
        }


        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //DrawerUtil.getDrawer(this, myToolbar);

        tratamientoPresenterImpl.obtenerTratamientos();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        final LayoutInflater inflater = this.getLayoutInflater();

        if (!esPaciente) {

            tratamientoView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View container, int i, long l) {
                    TableLayout linearLayoutParent = (TableLayout) container;

                    final TextView username = (TextView) container.findViewById(R.id.tratUsername);
                    final TextView desTratamiento = (EditText) container.findViewById(R.id.tratObservaciones);
                    final TextView titTratamiento = (TextView) container.findViewById(R.id.tratNombre);

                    AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                    builder.setMessage("¿Está seguro de eliminar el tratamiento seleccionado?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    tratamientoPresenterImpl.eliminarTratamiento(username.getText().toString()
                                            , titTratamiento.getText().toString(), desTratamiento.getText().toString());

                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Log.i(getString(R.string.tag_fisiopia), "[ProfesionalView] Cancel remove schedule");
                                    // User cancelled the dialog
                                }
                            }).show();

                    return true;


                }

            });

            btnAddTrat.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Log.i(getString(R.string.tag_fisiopia), "[TreatmentsView] Add treatment button selected");


                    final View dialogView = inflater.inflate(R.layout.dialog_newtratamiento, null);
                    builder.setView(dialogView);

                    final EditText txtTitTratamiento = (EditText) dialogView.findViewById(R.id.txtTitTratamiento);
                    final AutoCompleteTextView usernamePaciente = (AutoCompleteTextView) dialogView.findViewById(R.id.txtPaciente);
                    final EditText txtDesTratamiento = (EditText) dialogView.findViewById(R.id.txtDesTratamiento);

                    tratamientoPresenterImpl.obtenerUsernamesPacientes(usernamePaciente);
                    usernamePaciente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            usernameSelected = (String) parent.getItemAtPosition(position);
                        }
                    });

                    // Add action buttons
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {


                            Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Add treatment process - START");

                            if (!txtTitTratamiento.getText().toString().isEmpty() && !usernameSelected.toString().isEmpty() && !txtDesTratamiento.getText().toString().isEmpty()) {

                                tratamientoPresenterImpl.crearTratamiento(dialog, usernameSelected, txtTitTratamiento.getText().toString(), txtDesTratamiento.getText().toString());
                                //&tratamientoPresenterImpl.obtenerTratamientos();

                            } else {
                                mostrarMensaje("Lo siento, debe rellenar todos los datos.");
                            }

                            Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Add treatment process - END");


                        }
                    })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    Log.i(getString(R.string.tag_fisiopia), "[TreatmentsView] Add treatment process cancelled");
                                    dialog.dismiss();

                                }
                            });

                    AlertDialog b = builder.create();
                    b.show();


                }
            });
        }

        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Treatments view - END ");
    }


    public void logout() {
        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Logout process - START");
        // After logout redirect user to Loing Activity
        Intent i = new Intent(TratamientosActivity.this, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        startActivity(i);

        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Logout process - END");

    }

    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void visibilityProgressBar(int visibility) {
        progressBar.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.VISIBLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.GONE);
        }

    }

    public void mostrarTratamientos(List<Tratamiento> tratamientos) {

        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Show treatments process - START");

        Tratamiento[] tratamientoList;

        if (tratamientos == null || tratamientos.isEmpty()) {
            emptyTratamiento.setVisibility(View.VISIBLE);
            tratamientoList = new Tratamiento[0];
        } else {
            emptyTratamiento.setVisibility(View.GONE);
            tratamientoList = tratamientos.toArray(new Tratamiento[0]);
        }

        MyArrayAdapter adapter = new MyArrayAdapter(this, tratamientoList);
        tratamientoView.setAdapter(adapter);

        Log.d(getString(R.string.tag_fisiopia), "[TreatmentsView] Show treatments process - END");
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        return getParentActivityIntentImplement();
    }

    @Override
    public Intent getParentActivityIntent() {
        return getParentActivityIntentImplement();
    }

    private Intent getParentActivityIntentImplement() {
        Intent intent = null;

        // Determine which activity we came from with the bundle.
        if (sessionManager.getEsPaciente()) {
            intent = new Intent(this, PacienteMainActivity.class);
        } else {
            intent = new Intent(this, ProfesionalMainActivity.class);
        }

        return intent;
    }


}
