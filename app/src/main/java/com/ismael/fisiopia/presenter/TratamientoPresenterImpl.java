package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;
import android.widget.AutoCompleteTextView;

import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.async.GetTratamientos;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.TratamientosActivity;

import java.util.List;

/**
 * Created by Ismael on 12/10/2017.
 */

public class TratamientoPresenterImpl implements TratamientoPresenter{

    private TratamientosActivity mainActivity;
    private SessionManager sessionManager;
    private UsuarioService usuarioService;
    private boolean esPaciente;


    public TratamientoPresenterImpl(TratamientosActivity tratamientosActivity) {
        this.mainActivity = tratamientosActivity;
        this.sessionManager = new SessionManager(tratamientosActivity);
        this.usuarioService = new UsuarioService(tratamientosActivity);


    }

    public void visibilityProgressBar(int visibility){
        mainActivity.visibilityProgressBar(visibility);
    }

    @Override
    public void mostrarTratamientos(List<Tratamiento> tratamientos) {
        mainActivity.mostrarTratamientos(tratamientos);
    }

    public void obtenerTratamientos(){

        String username = sessionManager.getUsername();


        new GetTratamientos(username, mainActivity.getApplicationContext() , this).execute();
    }

    @Override
    public void logout() {
        // Clearing all data from Shared Preferences
        sessionManager.logoutUser();
        mainActivity.logout();
    }

    public void obtenerUsernamesPacientes(AutoCompleteTextView usernameTratamiento){

        String token = sessionManager.getToken();
        usuarioService.getUsernames(mainActivity,token,usernameTratamiento);
    }

    @Override
    public void crearTratamiento(DialogInterface dialog, String paciente, String titTratamiento, String desTratamiento) {
        Tratamiento tratamiento = new Tratamiento();

        tratamiento.setProfesional(sessionManager.getUsername());
        tratamiento.setPaciente(paciente);
        tratamiento.setDesTratamiento(desTratamiento);
        tratamiento.setTitTratamiento(titTratamiento);

        String token = sessionManager.getToken();
        usuarioService.createTratamiento(dialog,token, tratamiento, this);
    }

    @Override
    public void eliminarTratamiento(String paciente, String titTratamiento, String desTratamiento) {
        Tratamiento tratamiento = new Tratamiento();

        tratamiento.setProfesional(sessionManager.getUsername());
        tratamiento.setPaciente(paciente);
        tratamiento.setDesTratamiento(desTratamiento);
        tratamiento.setTitTratamiento(titTratamiento);

        String token = sessionManager.getToken();
        usuarioService.deleteTratamiento(token, tratamiento, this);
    }


}
