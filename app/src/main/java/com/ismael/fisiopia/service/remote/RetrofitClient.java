package com.ismael.fisiopia.service.remote;

/**
 * Created by Ismael on 17/11/2017.
 */

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder httpClient;
    public static Retrofit getClient(String baseUrl, final String token) {
            if( token != null && !token.isEmpty()){
                httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest  = chain.request().newBuilder()
                                .addHeader("Authorization", "token " + token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                });
            }else{
                httpClient = new OkHttpClient.Builder();
            }

            //Interceptor para logear las peticiones y respuestas
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        return retrofit;
    }
}
