package com.ismael.fisiopia.views;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.ProfMainPresenterImpl;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.util.DrawerUtil;
import com.ismael.fisiopia.util.HorarioArrayAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ProfesionalMainActivity extends AppCompatActivity {

    private ProfMainPresenterImpl profMainPresenterImpl;
    private ProgressBar progressBar;
    private FloatingActionButton btnAddHorario;
    private ListView horarioView;
    private RelativeLayout layout;
    private CheckBox lunes;
    private CheckBox martes;
    private CheckBox miercoles;
    private CheckBox jueves;
    private CheckBox viernes;
    private CheckBox sabado;
    private CheckBox domingo;
    private TextView emptyHorario;

    private static String username;
    private AlertDialog.Builder builder;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Profesional view - START ");


        super.onCreate(savedInstanceState);
        setContentView(R.layout.profmain_activity);
        profMainPresenterImpl = new ProfMainPresenterImpl(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMainProfesional);
        horarioView = (ListView) findViewById(R.id.horarioview);
        layout = (RelativeLayout) findViewById(R.id.progressLayoutMainProf);
        btnAddHorario = (FloatingActionButton) findViewById(R.id.btnAddHorario);
        emptyHorario = (TextView) findViewById(R.id.txtEmptyHorario);

        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        setTitle(R.string.title_schedule);


        if (getIntent().getExtras() != null) {
            username = getIntent().getExtras().getString("username");
            profMainPresenterImpl.obtenerDatosProfesional(username);
        }

        //Navigation drawer
        DrawerUtil.getDrawer(this, myToolbar, username);

        profMainPresenterImpl.cargarHorariosProfesional();

        builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        inflater = this.getLayoutInflater();


        btnAddHorario.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i(getString(R.string.tag_fisiopia), "[ProfesionalView] Add schedule button selected");

                final View dialogView = inflater.inflate(R.layout.rowhorario, null);
                builder.setView(dialogView);

                final EditText txtHourIni = (EditText) dialogView.findViewById(R.id.hourIni);
                final EditText txtHourEnd = (EditText) dialogView.findViewById(R.id.hourEnd);
                lunes = (CheckBox) dialogView.findViewById(R.id.checkLunes);
                martes = (CheckBox) dialogView.findViewById(R.id.checkMartes);
                miercoles = (CheckBox) dialogView.findViewById(R.id.checkMiercoles);
                jueves = (CheckBox) dialogView.findViewById(R.id.checkJueves);
                viernes = (CheckBox) dialogView.findViewById(R.id.checkViernes);
                sabado = (CheckBox) dialogView.findViewById(R.id.checkSabado);
                domingo = (CheckBox) dialogView.findViewById(R.id.checkDomingo);
                final TextView titulo = (TextView) dialogView.findViewById(R.id.txtAddHorario);

                titulo.setVisibility(View.VISIBLE);
                setClickableCheckbox();

                txtHourIni.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showTimePicker(txtHourIni);
                    }
                });

                txtHourEnd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showTimePicker(txtHourEnd);
                    }
                });

                // Add action buttons
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Add new schedule process - START");

                        List<String> dias = new ArrayList<String>();

                        if (lunes.isChecked()) {
                            dias.add("lunes");
                        }

                        if (martes.isChecked()) {
                            dias.add("martes");
                        }

                        if (miercoles.isChecked()) {
                            dias.add("miercoles");
                        }

                        if (jueves.isChecked()) {
                            dias.add("jueves");
                        }

                        if (viernes.isChecked()) {
                            dias.add("viernes");
                        }

                        if (sabado.isChecked()) {
                            dias.add("sabado");
                        }

                        if (domingo.isChecked()) {
                            dias.add("domingo");
                        }

                        String horaini = txtHourIni.getText().toString();
                        String horafin = txtHourEnd.getText().toString();
                        if (horaini != null && !horaini.isEmpty() && horafin != null && !horafin.isEmpty() && !dias.isEmpty()) {
                            profMainPresenterImpl.createHorario(horaini, horafin, dias);
                            profMainPresenterImpl.cargarHorariosProfesional();
                        } else {
                            mostrarMensaje("Debe seleccionar al menos un día, y configurar la hora de inicio y fin.");
                        }

                        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Add new schedule process - END");

                    }
                })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Cancel add a new schedule");
                                dialog.dismiss();

                            }
                        });

                AlertDialog b = builder.create();
                b.show();


            }
        });


        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Profesional view - END");
    }


    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void logout() {

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Logout process - START");
        // After logout redirect user to Loing Activity
        Intent i = new Intent(ProfesionalMainActivity.this, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        startActivity(i);

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Logout process - END");
    }

    private void showTimePicker(final EditText editTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                try {

                    String hour = selectedHour + ":" + selectedMinute;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    editTime.setText("" + new SimpleDateFormat("HH:mm", Locale.getDefault()).format(simpleDateFormat.parse(hour)));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, hour, minute, true);
        mTimePicker.setTitle("Seleccione una hora");
        mTimePicker.show();
    }

    public void visibilityProgressBar(int visibility) {
        progressBar.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.VISIBLE);

        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.GONE);

        }

    }

    public void mostrarHorarios(List<Horario[]> arrayHorarios) {

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Show schedule process - START");

        if (arrayHorarios.isEmpty()) {
            emptyHorario.setVisibility(View.VISIBLE);
        } else {
            emptyHorario.setVisibility(View.GONE);
        }


        HorarioArrayAdapter adapter = new HorarioArrayAdapter(this, arrayHorarios);
        horarioView.setAdapter(adapter);


        // Setting the item click listener for the listview
        horarioView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View container, int i, long l) {
                TableLayout linearLayoutParent = (TableLayout) container;

                CheckBox checkLunesDelete = (CheckBox) container.findViewById(R.id.checkLunes);
                CheckBox checkMartesDelete = (CheckBox) container.findViewById(R.id.checkMartes);
                CheckBox checkMiercolesDelete = (CheckBox) container.findViewById(R.id.checkMiercoles);
                CheckBox checkJuevesDelete = (CheckBox) container.findViewById(R.id.checkJueves);
                CheckBox checkViernesDelete = (CheckBox) container.findViewById(R.id.checkViernes);
                CheckBox checkSabadoDelete = (CheckBox) container.findViewById(R.id.checkSabado);
                CheckBox checkDomingoDelete = (CheckBox) container.findViewById(R.id.checkDomingo);

                final EditText horaIniDelete = (EditText) container.findViewById(R.id.hourIni);
                final EditText horaFinDelete = (EditText) container.findViewById(R.id.hourEnd);


                final List<String> diasDelete = new ArrayList<String>();

                if (checkLunesDelete.isChecked()) {
                    diasDelete.add("lunes");
                }

                if (checkMartesDelete.isChecked()) {
                    diasDelete.add("martes");
                }

                if (checkMiercolesDelete.isChecked()) {
                    diasDelete.add("miercoles");
                }

                if (checkJuevesDelete.isChecked()) {
                    diasDelete.add("jueves");
                }

                if (checkViernesDelete.isChecked()) {
                    diasDelete.add("viernes");
                }

                if (checkSabadoDelete.isChecked()) {
                    diasDelete.add("sabado");
                }

                if (checkDomingoDelete.isChecked()) {
                    diasDelete.add("domingo");
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                builder.setMessage("¿Está seguro de eliminar el horario seleccionado?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.i(getString(R.string.tag_fisiopia), "[ProfesionalView] Delete schedule");
                                profMainPresenterImpl.deleteHorario(horaIniDelete.getText().toString(), horaFinDelete.getText().toString(), diasDelete);
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.i(getString(R.string.tag_fisiopia), "[ProfesionalView] Cancel remove schedule");
                                // User cancelled the dialog
                            }
                        }).show();

                return true;

            }
        });

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Show schedule process - END");

    }

    public void setClickableCheckbox() {

        Log.d(getString(R.string.tag_fisiopia), "[ProfesionalView] Checkboxes disabled");
        lunes.setClickable(true);
        martes.setClickable(true);
        miercoles.setClickable(true);
        jueves.setClickable(true);
        viernes.setClickable(true);
        sabado.setClickable(true);
        domingo.setClickable(true);
    }

}
