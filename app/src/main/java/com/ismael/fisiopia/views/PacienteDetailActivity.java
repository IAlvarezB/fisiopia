package com.ismael.fisiopia.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacDetailPresenterImpl;
import com.ismael.fisiopia.util.GooglePlacesAutocompleteAdapter;


public class PacienteDetailActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private PacDetailPresenterImpl pacDetailPresenterImpl;

    private EditText txtNombre;
    private EditText txtApellidos;
    private AutoCompleteTextView txtDireccion;
    private EditText txtEmail;
    private EditText txtUsuario;
    private EditText txtDni;
    FloatingActionButton btnEditar;
    private ProgressBar progressBar;
    private RelativeLayout layout;

    private boolean esEditable = false;

    private String direccion = new String();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Patient detail view - START ");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pacdetail_activity);
        pacDetailPresenterImpl = new PacDetailPresenterImpl(this);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        final LayoutInflater inflater = this.getLayoutInflater();


        txtNombre = (EditText) findViewById(R.id.txtNombreDetail);
        txtApellidos = (EditText) findViewById(R.id.txtApellidosDetail);
        txtDireccion = (AutoCompleteTextView) findViewById(R.id.txtDireccionDetail);
        txtEmail = (EditText) findViewById(R.id.txtEmailDetail);
        txtUsuario = (EditText) findViewById(R.id.txtUsuarioDetail);
        txtDni = (EditText) findViewById(R.id.txtDNI);
        btnEditar = (FloatingActionButton) findViewById(R.id.btnEdit);
        Button btnChangePass = (Button) findViewById(R.id.btnCambiarPassPac);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        layout = (RelativeLayout) findViewById(R.id.progressLayoutDetailPac);
        TextView deleteUserLbl = (TextView) findViewById(R.id.lblDeleteUser);


        txtDireccion.setAdapter(new GooglePlacesAutocompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, true));
        txtDireccion.setOnItemClickListener(this);

        disabledEditTextDetail();

        //Se cargan los datos del paciente
        pacDetailPresenterImpl.mostrarDatosPaciente();

        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.title_profile);

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(getString(R.string.tag_fisiopia), "[PatientDetailView] Edit button of patient clicked");


                if (!esEditable) {
                    esEditable = true;
                    txtNombre.setEnabled(true);
                    txtApellidos.setEnabled(true);
                    txtDni.setEnabled(true);
                    txtDireccion.setEnabled(true);
                    txtEmail.setEnabled(true);
                    btnEditar.setImageResource(R.mipmap.ic_done_white_24dp);

                } else {

                    if(pacDetailPresenterImpl.modificarDatosPaciente(txtUsuario.getText().toString(), txtNombre.getText().toString(), txtApellidos.getText().toString(),
                            txtEmail.getText().toString(), txtDireccion.getText().toString(), txtDni.getText().toString())){
                        esEditable = false;
                        btnEditar.setImageResource(R.mipmap.ic_edit_white_24dp);

                    }

                }
            }
        });


        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(getString(R.string.tag_fisiopia), "[PatientDetailView] Change password button of patient clicked");


                builder.setView(inflater.inflate(R.layout.dialog_changepass, null))
                        // Add action buttons
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                final EditText txtOldPass = (EditText) ((AlertDialog) dialog).findViewById(R.id.txtOldpasswordChange);
                                final EditText txtNewPass = (EditText) ((AlertDialog) dialog).findViewById(R.id.txtNewpasswordChange);
                                final EditText txtNewPassAux = (EditText) ((AlertDialog) dialog).findViewById(R.id.txtNewpasswordChangeAux);

                                if (!txtOldPass.getText().toString().isEmpty() && !txtNewPass.getText().toString().isEmpty() && !txtNewPassAux.getText().toString().isEmpty()) {
                                    if (txtNewPass.getText().toString().equals(txtNewPassAux.getText().toString())) {
                                        pacDetailPresenterImpl.changePass(dialog, txtOldPass.getText().toString(), txtNewPass.getText().toString());
                                    } else {
                                        mostrarMensaje("Las nuevas contraseñas no son iguales");
                                    }

                                } else {
                                    mostrarMensaje("Debe rellenar todos los datos.");
                                }


                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                            }
                        }).show();

            }
        });


        deleteUserLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDialogConfirmar();
            }
        });

        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Patient detail view - END ");


    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        direccion = (String) adapterView.getItemAtPosition(position);
    }

    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void logout() {
        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Logout - START");
        // After logout redirect user to Loing Activity
        Intent i = new Intent(PacienteDetailActivity.this, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        startActivity(i);

        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Logout - END");

    }

    public void mostrarDatosPaciente(String username, String nombre, String apellidos, String email, String direccion, String dni) {
        txtUsuario.setText(username);
        txtNombre.setText(nombre);
        txtApellidos.setText(apellidos);
        txtDireccion.setText(direccion);
        txtEmail.setText(email);
        txtDni.setText(dni);
    }

    public void setimageBotonEditar(int icon) {
        btnEditar.setImageResource(icon);
    }

    public void visibilityProgressBar(int visibility) {

        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] visibilityProgressBar ");

        progressBar.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            btnEditar.setEnabled(false);
            layout.setVisibility(View.VISIBLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            btnEditar.setEnabled(true);
            layout.setVisibility(View.GONE);
        }

    }


    public void disabledEditTextDetail() {

        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Disabled texts ");

        txtDireccion.setEnabled(false);
        txtNombre.setEnabled(false);
        txtApellidos.setEnabled(false);
        txtEmail.setEnabled(false);
        txtUsuario.setEnabled(false);
        txtDni.setEnabled(false);
    }

    private void mostrarDialogConfirmar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Está seguro de eliminar tu cuenta?. Después de esto no habrá marcha atrás, se eliminarán todos tus datos almacenados")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Log.d(getString(R.string.tag_fisiopia), "[PatientDetailView] Removed user ");

                        pacDetailPresenterImpl.deletePaciente();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                }).show();
    }

}
