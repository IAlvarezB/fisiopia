package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.ProfMainPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class CargarHorarios extends AsyncTask<Void, Integer, List<Horario>> {

    private SessionManager sessionManager;
    private ProfMainPresenterImpl profMainPresenter;
    private Context context;
    private String username;

    public CargarHorarios(Context context, ProfMainPresenterImpl profMainPresenterImpl, String username) {

        this.sessionManager = new SessionManager(context);
        this.profMainPresenter = profMainPresenterImpl;
        this.context = context;
        this.username = username;
    }


    @Override
    protected void onPreExecute() {
        Log.d(context.getString(R.string.tag_fisiopia), "[CargarHorarios] Get schedule service - START");

        profMainPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected List<Horario> doInBackground(Void... voids) {

        List<Horario> result = new ArrayList<Horario>();

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());

        if(username == null || username.isEmpty()){
            username = sessionManager.getUsername();
        }

        Call<List<Horario>> call = mAPIService.getHorarioByUsername(username);

        try {
            result = call.execute().body();

        } catch (IOException e) {
            e.printStackTrace();
            result = null;
            Log.e(context.getString(R.string.tag_fisiopia), "[CargarHorarios] Get schedule service ERROR!: "+username, e);
        }


        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        profMainPresenter.visibilityProgressBar(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(List<Horario> result) {
        profMainPresenter.visibilityProgressBar(View.GONE);
        profMainPresenter.mostrarHorarios(result);

        Log.d(context.getString(R.string.tag_fisiopia), "[CargarHorarios] Get schedule service - END");
    }

}
