
package com.ismael.fisiopia.service.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationResult {

    @SerializedName("results")
    @Expose
    private List<Result> results = null;
    /**
     * The Status Schema 
     * <p>
     * 
     * 
     */
    @SerializedName("status")
    @Expose
    private String status = "";

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    /**
     * The Status Schema 
     * <p>
     * 
     * 
     */
    public String getStatus() {
        return status;
    }

    /**
     * The Status Schema 
     * <p>
     * 
     * 
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
