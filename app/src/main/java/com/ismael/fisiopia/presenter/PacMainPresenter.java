package com.ismael.fisiopia.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.ismael.fisiopia.service.async.CargarHorarios;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Profesional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface PacMainPresenter {

    void obtenerDatosPaciente(String username);

    void visibilityProgressBar(int visibility);

    void logout();

    void mostrarMarkers(List<Profesional> profesionales);

    void cargarHorariosProfesional(String usernameProfesional);

    void mostrarHorarios(List<Horario> horarios);

    HashMap<Integer, List<Horario>> dividirHorarios(List<Horario> horarios);


}