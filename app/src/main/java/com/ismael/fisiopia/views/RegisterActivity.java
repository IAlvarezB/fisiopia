package com.ismael.fisiopia.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.dto.UserDto;
import com.ismael.fisiopia.presenter.RegisterPresenter;
import com.ismael.fisiopia.presenter.RegisterPresenterImpl;
import com.ismael.fisiopia.util.GooglePlacesAutocompleteAdapter;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private Boolean paciente;
    private String direccion = new String();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(getString(R.string.tag_fisiopia), "[RegisterView] Register view - START ");


        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle("Nuevo usuario");
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.title_new_user);

        final RegisterPresenterImpl registerPresenter= new RegisterPresenterImpl(this);

        final EditText txtUsername = (EditText) findViewById(R.id.txtUserName);
        final EditText txtPass = (EditText) findViewById(R.id.txtPassword);
        final EditText txtNombre = (EditText) findViewById(R.id.txtNombre);
        final EditText txtApellidos = (EditText) findViewById(R.id.txtApellidos);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtEmailConfirm = (EditText) findViewById(R.id.txtEmailConfirm);
        final RadioButton radioFisio = (RadioButton) findViewById(R.id.radioFisio);
        final RadioButton radioPaciente = (RadioButton) findViewById(R.id.radioPaciente);
        final TableLayout panelDireccion = (TableLayout) findViewById(R.id.layoutDireccion);
        final EditText txtNumColegiado = (EditText) findViewById(R.id.txtNumColegiado);
        final Button btnRegistro = (Button) findViewById(R.id.btnRegistro);
        final AutoCompleteTextView autoDireccion = (AutoCompleteTextView) findViewById(R.id.txtDireccion);
        final EditText numeroDireccion = (EditText) findViewById(R.id.txtNumeroDireccion);
        final EditText txtDni = (EditText) findViewById(R.id.txtDni);


        //En el caso que el nuevo usuario se desee dar de alta como Fisioterapeuta
        radioFisio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i(getString(R.string.tag_fisiopia), "[RegisterView] Profesional profile clicked ");

                paciente = false;
                txtNumColegiado.setVisibility(View.VISIBLE);
                panelDireccion.setVisibility(View.VISIBLE);

            }
        });
        //En el caso que el nuevo usuario se desee dar de alta como Paciente
        radioPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(getString(R.string.tag_fisiopia), "[RegisterView] Patient profile clicked ");

                paciente = true;
                txtNumColegiado.setVisibility(View.GONE);
                panelDireccion.setVisibility(View.VISIBLE);
            }
        });

        autoDireccion.setAdapter(new GooglePlacesAutocompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, true));
        autoDireccion.setOnItemClickListener(this);

        //Listener para el boton de registro
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(getString(R.string.tag_fisiopia), "[RegisterView] Create a new user process - START");
                UserDto userDto = new UserDto(txtUsername.getText().toString(),
                        txtPass.getText().toString(),
                        txtNombre.getText().toString(),
                        txtApellidos.getText().toString(),
                        txtEmail.getText().toString(),
                        direccion,
                        txtNumColegiado.getText().toString(),
                        paciente,
                        Integer.valueOf(numeroDireccion.getText().toString()),
                        txtDni.getText().toString());

                //Se llama al presenter donde se realiza toda la lógica de negocio
                registerPresenter.crearUsuario(userDto,txtEmailConfirm.getText().toString());
                Log.d(getString(R.string.tag_fisiopia), "[RegisterView] Create a new user process - END");
            }
        });

        Log.d(getString(R.string.tag_fisiopia), "[RegisterView] Register view - END ");
    }
    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        direccion = (String) adapterView.getItemAtPosition(position);
    }

    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
