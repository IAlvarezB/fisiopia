package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;
import android.widget.AutoCompleteTextView;

import com.ismael.fisiopia.service.model.Tratamiento;

import java.util.List;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface TratamientoPresenter {

    void obtenerTratamientos();

    void visibilityProgressBar(int visibility);

    void mostrarTratamientos(List<Tratamiento> tratamientos);

    void logout();

    void obtenerUsernamesPacientes(AutoCompleteTextView usernameTratamiento);

    void crearTratamiento(DialogInterface dialog, String paciente, String titTratamiento, String desTratamiento);

    void eliminarTratamiento(String paciente, String titTratamiento, String desTratamiento);


}


