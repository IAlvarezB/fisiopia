package com.ismael.fisiopia.service;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.ProfMainPresenterImpl;
import com.ismael.fisiopia.presenter.TratamientoPresenterImpl;
import com.ismael.fisiopia.service.async.CargarHorarios;
import com.ismael.fisiopia.service.async.GetTratamientos;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.LoginActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ismael on 17/11/2017.
 */

public class ProfesionalService {

    private APIService mAPIService;
    private SessionManager sessionManager;
    Context context;

    public ProfesionalService(Context context) {
        super();
        sessionManager = new SessionManager(context);
        this.context = context;
    }

    public void crearProfesional(Profesional profesional) {

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Create a new profesional process - START");

        mAPIService = ApiUtils.getAPIService(null);
        mAPIService.saveProfesional(profesional).enqueue(new Callback<Profesional>() {
            @Override
            public void onResponse(Call<Profesional> call, Response<Profesional> response) {

                if (response.isSuccessful()) {
                    //Exito
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    context.startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<Profesional> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Create a new profesional process ERROR!",t);
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Create a new profesional process - END");
    }

    public void obtenerProfesional(String username, String token) {

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Get profesional process - START");
        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.getProfesionalByUsername(username).enqueue(new Callback<Profesional>() {
            @Override
            public void onResponse(Call<Profesional> call, Response<Profesional> response) {

                if (response.isSuccessful()) {
                    Profesional profesional = response.body();
                    sessionManager.createLoginProfesionalSession(profesional);
                }
            }

            @Override
            public void onFailure(Call<Profesional> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Get profesional process ERROR!",t);
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Get profesional process - END");
    }

    public void deleteProfesional(String username, String token) {

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete profesional process - START");
        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.deleteProfesional(username).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful()) {
                    sessionManager.logoutUser();
                    //Exito
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    context.startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete profesional process ERROR!",t);
                Toast.makeText(context, "Error al dar de baja al usuario, inténtelo más tarde", Toast.LENGTH_LONG).show();
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete profesional process - END");
    }

    public void deleteHorario (String token, Horario horario){

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete schedule process - START");

        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.deleteHorario(horario).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful()){
                    Toast.makeText(context, "Horario eliminado correctamente", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(context, "Error al eliminar el horario, inténtelo más tarde", Toast.LENGTH_LONG).show();
                    Log.e("[FISIOPIA]","Error en la respuesta al eliminar el horario");

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete schedule process ERROR!",t);
                Toast.makeText(context, "Error al eliminar el horario, inténtelo mas tarde", Toast.LENGTH_LONG).show();

            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[ProfesionalService] Delete schedule process - END");
    }

}
