package com.ismael.fisiopia.util;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.ismael.fisiopia.service.GoogleAPIService;
import com.ismael.fisiopia.service.UsuarioService;

import java.util.ArrayList;

/**
 * Created by ismaelalvarez on 6/12/17.
 */

public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
    boolean address;
    private ArrayList<String> resultList;
    UsuarioService usuarioService;

    public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId, Boolean address) {
        super(context, textViewResourceId);
        this.address = address;
        this.usuarioService = new UsuarioService(context);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    if(address){
                        resultList = usuarioService.autocompleteRegister("address",constraint.toString(),"country:esp");
                    }else{

                    }

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }
}


