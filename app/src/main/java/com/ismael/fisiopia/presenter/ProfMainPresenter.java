package com.ismael.fisiopia.presenter;

import com.ismael.fisiopia.service.model.Horario;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface ProfMainPresenter {

    void logout();

    void createHorario(String horaini, String horafin, List<String> dias);

    void cargarHorariosProfesional();

    void visibilityProgressBar(int visibility);

    void mostrarHorarios(List<Horario> horarios);

    HashMap<Integer, List<Horario>> dividirHorarios(List<Horario> horarios);

    void deleteHorario(String horaIni, String horaFin, List<String> dias);


}