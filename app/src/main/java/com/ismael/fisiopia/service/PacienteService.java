package com.ismael.fisiopia.service;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacDetailPresenter;
import com.ismael.fisiopia.presenter.PacDetailPresenterImpl;
import com.ismael.fisiopia.presenter.RegisterPresenter;
import com.ismael.fisiopia.presenter.RegisterPresenterImpl;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.LoginActivity;
import com.ismael.fisiopia.views.RegisterActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ismael on 17/11/2017.
 */

public class PacienteService {

    private APIService mAPIService;
    private SessionManager sessionManager;
    private Context context;

    public PacienteService(Context context)
    {
        super();
        sessionManager = new SessionManager(context);
        this.context = context;
    }

    public void crearPaciente(Paciente paciente) {

        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Create a new patient process - START");

        //null por que no hace falta token para crear un profesional
        mAPIService = ApiUtils.getAPIService(null);
        mAPIService.savePaciente(paciente).enqueue(new Callback<Paciente>() {
            @Override
            public void onResponse(Call<Paciente> call, Response<Paciente> response) {

                if(response.isSuccessful()) {
                    //Exito
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    context.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Paciente> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[PacienteService] Create a new patient process ERROR", t);
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Create a new patient process - END");

    }


    public void obtenerPaciente (String username, String token){
        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Get a patient process - START");


        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.getPacienteByUsername(username).enqueue(new Callback<Paciente>() {
            @Override
            public void onResponse(Call<Paciente> call, Response<Paciente> response) {

                if(response.isSuccessful()) {
                    Paciente paciente = response.body();
                    sessionManager.createLoginPacienteSession(paciente);
                }
            }

            @Override
            public void onFailure(Call<Paciente> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[PacienteService] Get a patient process ERROR", t);
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Get a patient process - END");
    }

    public void deletePaciente (String username, String token) {

        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Delete a patient process - START");
        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.deletePaciente(username).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful()) {

                    sessionManager.logoutUser();
                    //Exito
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    context.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(context.getString(R.string.tag_fisiopia), "[PacienteService] Delete a patient process ERROR", t);
                Toast.makeText(context, "Error al dar de baja al usuario, inténtelo más tarde", Toast.LENGTH_LONG).show();
                //Error
            }
        });

        Log.d(context.getString(R.string.tag_fisiopia), "[PacienteService] Delete a patient process - END");
    }
}
