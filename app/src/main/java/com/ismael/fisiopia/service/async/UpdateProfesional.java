package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.ProfDetailPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class UpdateProfesional extends AsyncTask <Void, Integer, Boolean>{

    private Profesional profesional;
    private SessionManager sessionManager;
    private ProfDetailPresenterImpl profDetailPresenter;
    private Context context;

    public UpdateProfesional(Profesional profesional, Context context, ProfDetailPresenterImpl profesionalDetailActivity) {
        this.profesional = profesional;
        this.sessionManager = new SessionManager(context);
        this.profDetailPresenter = profesionalDetailActivity;
        this.context = context;
    }


    @Override
    protected void onPreExecute()
    {

        Log.d(context.getString(R.string.tag_fisiopia), "[UpdateProfesional] Update profesional service - START");

        profDetailPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        boolean result= false;

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());
        Call<Profesional> call = mAPIService.updateProfesional(profesional);

        try {
            Profesional profesionalModificate = call.execute().body();
            if(profesionalModificate != null){
                sessionManager.createLoginProfesionalSession(profesionalModificate);
                result = true;
            }else{
                result = false;

            }

        } catch (IOException e) {
            e.printStackTrace();
            result = false;
            Log.e(context.getString(R.string.tag_fisiopia), "[UpdateProfesional] Update profesional service ERROR!", e);

        }

        return result;
    }
    @Override
    protected void onProgressUpdate(Integer... values)
    {
        profDetailPresenter.visibilityProgressBar(View.VISIBLE);
    }
    @Override
    protected void onPostExecute(Boolean result)
    {
        profDetailPresenter.visibilityProgressBar(View.GONE);

        if(!result){
            profDetailPresenter.mostrarMensaje("Error al actualizar los datos. Inténtelo más tarde");
        }else{
            profDetailPresenter.cambiarIconoDetail(R.mipmap.ic_edit_white_24dp);
            profDetailPresenter.mostrarMensaje("Datos acutalizados correctamente");
            profDetailPresenter.disabledEditTextDetail();
        }

        Log.d(context.getString(R.string.tag_fisiopia), "[UpdateProfesional] Update profesional service - END");


    }

}
