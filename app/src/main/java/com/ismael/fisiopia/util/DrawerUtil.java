package com.ismael.fisiopia.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.views.LoginActivity;
import com.ismael.fisiopia.views.PacienteDetailActivity;
import com.ismael.fisiopia.views.PacienteMainActivity;
import com.ismael.fisiopia.views.ProfesionalDetailActivity;
import com.ismael.fisiopia.views.ProfesionalMainActivity;
import com.ismael.fisiopia.views.TratamientosActivity;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

/**
 * Created by ismaelalvarez on 16/03/18.
 */

public class DrawerUtil {
    public static void getDrawer(final Activity activity, Toolbar toolbar, String username) {
        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem drawerEmptyItem = new PrimaryDrawerItem().withIdentifier(0).withName("");
        drawerEmptyItem.withEnabled(false);
        final SessionManager sessionManager = new SessionManager(activity);
        PrimaryDrawerItem drawerHome = new PrimaryDrawerItem().withIdentifier(1)
                .withName(R.string.menu_perfil).withIcon(R.mipmap.ic_face_black_24dp);
        PrimaryDrawerItem drawerTratamientos = new PrimaryDrawerItem().withIdentifier(2)
                .withName(R.string.menu_tratamiento).withIcon(R.mipmap.ic_accessibility_black_24dp);

        SecondaryDrawerItem drawerAbout = new SecondaryDrawerItem().withIdentifier(3)
                .withName(R.string.menu_acercade).withIcon(R.mipmap.ic_info_black_24dp);
        SecondaryDrawerItem drawerLogout = new SecondaryDrawerItem().withIdentifier(4)
                .withName(R.string.menu_logout).withIcon(R.mipmap.ic_exit_to_app_black_24dp);

        //Drawer header
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.background_nav_header)
                .addProfiles(
                        new ProfileDrawerItem().withName((username==null)?"":username).withIcon(R.mipmap.ic_account_circle_white)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();


        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withAccountHeader(headerResult)
                .withActivity(activity)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withCloseOnClick(true)
                .withSelectedItem(-1)
                .addDrawerItems(
                        drawerHome,
                        drawerTratamientos,
                        new DividerDrawerItem(),
                        drawerAbout,
                        drawerLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(final View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem.getIdentifier() == 1 && (activity instanceof PacienteMainActivity)) {
                            Intent intent = new Intent(activity, PacienteDetailActivity.class);
                            view.getContext().startActivity(intent);
                        } else if (drawerItem.getIdentifier() == 2 && (activity instanceof PacienteMainActivity)) {
                            Intent intent = new Intent(activity, TratamientosActivity.class);
                            view.getContext().startActivity(intent);
                        } else if (drawerItem.getIdentifier() == 3 && (activity instanceof PacienteMainActivity)) {
                            // TODO: ABOUT
                        } else if (drawerItem.getIdentifier() == 4 && (activity instanceof PacienteMainActivity)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage("¿Está seguro de cerrar la sesión?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            sessionManager.logoutUser();
                                            Intent i = new Intent(activity, LoginActivity.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            view.getContext().startActivity(i);
                                        }
                                    })
                                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog
                                        }
                                    }).show();


                        } else if (drawerItem.getIdentifier() == 1 && (activity instanceof ProfesionalMainActivity)) {
                            Intent intent = new Intent(activity, ProfesionalDetailActivity.class);
                            view.getContext().startActivity(intent);
                        } else if (drawerItem.getIdentifier() == 2 && (activity instanceof ProfesionalMainActivity)) {
                            Intent intent = new Intent(activity, TratamientosActivity.class);
                            view.getContext().startActivity(intent);
                        } else if (drawerItem.getIdentifier() == 3 && (activity instanceof ProfesionalMainActivity)) {
                            // TODO: ABOUT
                        } else if (drawerItem.getIdentifier() == 4 && (activity instanceof ProfesionalMainActivity)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage("¿Está seguro de cerrar la sesión?")
                                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            sessionManager.logoutUser();
                                            Intent i = new Intent(activity, LoginActivity.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            view.getContext().startActivity(i);
                                        }
                                    })
                                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog
                                        }
                                    }).show();

                        }

                        return true;
                    }
                })
                .build();
    }
}
