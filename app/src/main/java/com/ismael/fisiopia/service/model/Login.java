package com.ismael.fisiopia.service.model;

/**
 * Created by Ismael on 17/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("esPaciente")
    @Expose
    private boolean esPaciente;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isEsPaciente() {
        return esPaciente;
    }

    public void setEsPaciente(boolean esPaciente) {
        this.esPaciente = esPaciente;
    }

    @Override
    public String toString() {
        return "Login{" +
                "token='" + token + '\'' +
                ", esPaciente=" + esPaciente +
                '}';
    }
}