package com.ismael.fisiopia.presenter;

import com.ismael.fisiopia.dto.UserDto;
import com.ismael.fisiopia.service.PacienteService;
import com.ismael.fisiopia.service.ProfesionalService;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.views.RegisterActivity;

/**
 * Created by Ismael on 12/10/2017.
 */

public class RegisterPresenterImpl implements RegisterPresenter {


    private RegisterActivity registerActivity;
    private PacienteService pacienteService;
    private ProfesionalService profesionalService;

    public RegisterPresenterImpl(RegisterActivity registerActivity) {

        this.registerActivity = registerActivity;
        this.pacienteService = new PacienteService(registerActivity);
        this.profesionalService = new ProfesionalService(registerActivity);
    }

    private boolean validacionRegistro(UserDto userDto, String confirmEmail) {

        String mensaje;
        boolean result;
        //En el caso de ser paciente no se controla el numero de colegiado
        if(userDto.isEsPaciente()){
            if (userDto.getUsername().equals("") || userDto.getPassword().equals("") || userDto.getNombre().equals("") || userDto.getApellidos().equals("")
                    || userDto.getEmail().equals("") || userDto.getPassword().equals("") || userDto.getDireccion().equals("") || userDto.getNumero().equals("")
                    || confirmEmail.equals("") || userDto.getDni().equals("")){


                mensaje = "Debe rellenar todos los datos para poder darse de alta en FISIOPIA";
                registerActivity.mostrarMensaje(mensaje);
                result = false;

            }else if(!userDto.getEmail().equals(confirmEmail)){

                mensaje = "Las direcciones de correo electrónico no coinciden";
                registerActivity.mostrarMensaje(mensaje);
                result = false;

            }else{
                result = true;
            }

            //En el caso de ser profesional si se controla el numero de colegiado
        }else{

            if (userDto.getUsername().equals("") || userDto.getPassword().equals("") || userDto.getNombre().equals("") || userDto.getApellidos().equals("")
                    || userDto.getEmail().equals("") || userDto.getPassword().equals("") || userDto.getDireccion().equals("") || userDto.getNumero().equals("")
                    || confirmEmail.equals("") || userDto.getNumColegiado().equals("") || userDto.getDni().equals("")){

                mensaje = "Debe rellenar todos los datos para poder darse de alta en FISIOPIA";
                registerActivity.mostrarMensaje(mensaje);

                result = false;

            }else if(!userDto.getEmail().equals(confirmEmail)){

                mensaje = "Las direcciones de correo electrónico no coinciden";
                registerActivity.mostrarMensaje(mensaje);
                result=false;
            }else{
                result = true;
            }

        }
        return result;
    }

    @Override
    public void crearUsuario(UserDto userDto, String txtEmailConfirm) {
        User user = new User();

        //En el caso que el usuario rellene correctamente todos los datos necesarios
        if(validacionRegistro(userDto, txtEmailConfirm)){
            //Una vez validado, creamos el objeto user con los datos recibidos
            user.setUsername(userDto.getUsername());
            user.setFirstName(userDto.getNombre());
            user.setLastName(userDto.getApellidos());
            user.setEmail(userDto.getEmail());
            user.setPassword(userDto.getPassword());

            //llamamos al presenter para que se encargue de la lógica de negocio.
            if(userDto.isEsPaciente()){
                Paciente paciente = new Paciente();
                paciente.setUser(user);
                paciente.setDireccion(userDto.getDireccion());
                paciente.setDni(userDto.getDni());
                pacienteService.crearPaciente(paciente);
            }else{
                Profesional profesional = new Profesional();
                profesional.setUser(user);
                profesional.setNumColegiado(userDto.getNumColegiado());
                profesional.setDni(userDto.getDni());
                profesional.setDireccion(userDto.getDireccion());
                profesionalService.crearProfesional(profesional);
            }

        }
    }

}
