package com.ismael.fisiopia.service;

/**
 * Created by Ismael on 17/11/2017.
 */

import com.ismael.fisiopia.service.model.LocationResult;
import com.ismael.fisiopia.service.model.PlacesResults;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//API_KEY=AIzaSyBwkeIBOghcVGrokY4ynk6shs7_KLwnkl8
public interface GoogleAPIService {

    @GET("/maps/api/place/autocomplete/json")
    Call<PlacesResults> getAddressResults(@Query("types") String types, @Query("input") String input, @Query("components") String components, @Query("key") String key);

    @GET("/maps/api/geocode/json")
    Call<LocationResult> getLocationByAddress(@Query("address") String address, @Query("key") String key);


}
