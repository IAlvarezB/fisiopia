package com.ismael.fisiopia.presenter;

import android.util.Log;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.async.LoginAsync;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.views.LoginActivity;

/**
 * Created by Ismael on 12/10/2017.
 */

public class LoginPresenterImpl implements LoginPresenter{


    private LoginActivity loginActivity;
    private UsuarioService usuarioService;

    public LoginPresenterImpl(LoginActivity loginActivity) {

        this.loginActivity = loginActivity;
        usuarioService = new UsuarioService(loginActivity);
    }


    @Override
    public void login(String username, String password) {

        Log.d(loginActivity.getString(R.string.tag_fisiopia), "[LoginPresenter] Login - START");

        User usuario = new User();
        usuario.setUsername(username);
        usuario.setPassword(password);

        new LoginAsync(usuario, loginActivity.getApplicationContext(), this).execute();

        Log.d(loginActivity.getString(R.string.tag_fisiopia), "[LoginPresenter] Login - END");

    }


    public void visibilityProgressBar(int visibility){
        loginActivity.visibilityProgressBar(visibility);
    }

    public void mostrarMensaje(String mensaje){
        loginActivity.mostrarMensaje(mensaje);
    }
}
