package com.ismael.fisiopia.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.model.Horario;

import java.util.List;

/**
 * Created by ismaelalvarez on 1/01/18.
 */

public class HorarioArrayAdapter extends ArrayAdapter<Horario[]> {
    private final Context context;
    private final List<Horario[]> horarios;

    public HorarioArrayAdapter(Context context, List<Horario[]> horarios) {
        super(context, R.layout.rowhorario, horarios);
        this.context = context;
        this.horarios = horarios;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowhorario, parent, false);
        final EditText txtHourIni = (EditText) rowView.findViewById(R.id.hourIni);
        final EditText txtHourEnd = (EditText) rowView.findViewById(R.id.hourEnd);
        final CheckBox lunes = (CheckBox) rowView.findViewById(R.id.checkLunes);
        final CheckBox martes = (CheckBox) rowView.findViewById(R.id.checkMartes);
        final CheckBox miercoles = (CheckBox) rowView.findViewById(R.id.checkMiercoles);
        final CheckBox jueves = (CheckBox) rowView.findViewById(R.id.checkJueves);
        final CheckBox viernes = (CheckBox) rowView.findViewById(R.id.checkViernes);
        final CheckBox sabado = (CheckBox) rowView.findViewById(R.id.checkSabado);
        final CheckBox domingo = (CheckBox) rowView.findViewById(R.id.checkDomingo);
        final TextView titulo = (TextView) rowView.findViewById(R.id.txtAddHorario);

        titulo.setVisibility(View.GONE);

        Horario[] horario = horarios.get(position);

        for (Horario item : horario) {
            switch (item.getDia()) {
                case "lunes":
                    lunes.setChecked(true);
                    break;
                case "martes":
                    martes.setChecked(true);
                    break;
                case "miercoles":
                    miercoles.setChecked(true);
                    break;
                case "jueves":
                    jueves.setChecked(true);
                    break;
                case "viernes":
                    viernes.setChecked(true);
                    break;
                case "sabado":
                    sabado.setChecked(true);
                    break;
                case "domingo":
                    domingo.setChecked(true);
                    break;
            }
        }

        txtHourIni.setText(horario[0].getHoraini());
        txtHourEnd.setText(horario[0].getHorafin());

        return rowView;
    }


}
