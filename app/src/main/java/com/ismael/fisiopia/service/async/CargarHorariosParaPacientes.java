package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacMainPresenterImpl;
import com.ismael.fisiopia.presenter.ProfMainPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class CargarHorariosParaPacientes extends AsyncTask<Void, Integer, List<Horario>> {

    private SessionManager sessionManager;
    private PacMainPresenterImpl pacMainPresenterImpl;
    private Context context;
    private String username;

    public CargarHorariosParaPacientes(Context context, PacMainPresenterImpl pacMainPresenterImpl, String username) {

        this.sessionManager = new SessionManager(context);
        this.pacMainPresenterImpl = pacMainPresenterImpl;
        this.context = context;
        this.username = username;
    }


    @Override
    protected void onPreExecute() {
        Log.d(context.getString(R.string.tag_fisiopia), "[CargarHorariosParaPacientes] Get schedule for patients service - START");

        super.onPreExecute();
    }

    @Override
    protected List<Horario> doInBackground(Void... voids) {

        List<Horario> result = new ArrayList<Horario>();

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());

        if(username == null || username.isEmpty()){
            username = sessionManager.getUsername();
        }

        Call<List<Horario>> call = mAPIService.getHorarioByUsername(username);

        try {
            result = call.execute().body();

        } catch (IOException e) {
            e.printStackTrace();
            result = null;
            Log.e(context.getString(R.string.tag_fisiopia), "[CargarHorariosParaPacientes] Get schedule for patients service ERROR!", e);
        }


        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
    }

    @Override
    protected void onPostExecute(List<Horario> result) {
        pacMainPresenterImpl.mostrarHorarios(result);
        Log.d(context.getString(R.string.tag_fisiopia), "[CargarHorariosParaPacientes] Get schedule for patients service - END");
    }

}
