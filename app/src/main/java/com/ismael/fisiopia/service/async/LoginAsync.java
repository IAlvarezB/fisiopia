package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.LoginPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Login;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.PacienteMainActivity;
import com.ismael.fisiopia.views.ProfesionalMainActivity;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class LoginAsync extends AsyncTask<Void, Integer, Boolean> {

    private User user;
    private Context appContext;
    private SessionManager sessionManager;
    private LoginPresenterImpl loginPresenter;

    public LoginAsync(User user, Context context, LoginPresenterImpl loginPresenter) {
        this.user = user;
        this.sessionManager = new SessionManager(context);
        this.appContext = context.getApplicationContext();
        this.loginPresenter = loginPresenter;

    }


    @Override
    protected void onPreExecute()
    {
        Log.d(appContext.getString(R.string.tag_fisiopia), "[LoginAsync] Login service - START");

        loginPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        Intent main;
        Boolean result = false;
        APIService mAPIService = ApiUtils.getAPIService(null);
        Call<User> call = mAPIService.login(user);

        try {
            User userLogin = call.execute().body();
            if(userLogin != null){

                if(userLogin.getEsPaciente()){
                    sessionManager.setToken(userLogin.getToken());
                    sessionManager.putUsername(user.getUsername());
                    main = new Intent(appContext, PacienteMainActivity.class);
                    main.putExtra("username", user.getUsername());
                }else{
                    sessionManager.setToken(userLogin.getToken());
                    sessionManager.putUsername(user.getUsername());
                    main = new Intent(appContext, ProfesionalMainActivity.class);
                    main.putExtra("username", user.getUsername());

                }
                result = true;
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                appContext.startActivity(main);
            }else{
                result = false;

            }

        } catch (IOException e) {
            Log.e(appContext.getString(R.string.tag_fisiopia), "[LoginAsync] Login service ERROR!", e);
            result = false;

        }
     return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        loginPresenter.visibilityProgressBar(View.VISIBLE);
    }
    @Override
    protected void onPostExecute(Boolean result)
    {
        loginPresenter.visibilityProgressBar(View.GONE);

        if(!result){
            loginPresenter.mostrarMensaje("Usuario/Contraseña incorrectas");
        }

        Log.d(appContext.getString(R.string.tag_fisiopia), "[LoginAsync] Login service - END");

    }

}
