package com.ismael.fisiopia.service.model;

/**
 * Created by Ismael on 17/11/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.TimeZone;

public class Horario {

    @SerializedName("profesional")
    @Expose
    private String profesional;
    @SerializedName("dia")
    @Expose
    private String dia;
    @SerializedName("horaini")
    @Expose
    private String horaini;
    @SerializedName("horafin")
    @Expose
    private String horafin;

    public String getProfesional() {
        return profesional;
    }

    public void setProfesional(String profesional) {
        this.profesional = profesional;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHoraini() {
        return horaini;
    }

    public void setHoraini(String horaini) {
        this.horaini = horaini;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    @Override
    public String toString() {
        return "Horario{" +
                "profesional='" + profesional + '\'' +
                ", dia='" + dia + '\'' +
                ", horaini='" + horaini + '\'' +
                ", horafin='" + horafin + '\'' +
                '}';
    }
}
