package com.ismael.fisiopia.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.LoginPresenterImpl;
import com.ismael.fisiopia.util.DrawerUtil;

public class LoginActivity extends AppCompatActivity {

    private LoginPresenterImpl loginPresenterImpl;
    ProgressBar progressBarLogin;
    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(getString(R.string.tag_fisiopia), "Login View- START");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        final EditText txtUsername = (EditText) findViewById(R.id.txtUserNameLogin);
        final EditText txtPass = (EditText) findViewById(R.id.txtPassLogin);
        final Button btnLogin = (Button) findViewById(R.id.btnLogin);
        final TextView txtRegistro = (TextView) findViewById(R.id.txtRegistroNuevo);
        progressBarLogin = (ProgressBar) findViewById(R.id.progressBarLogin);
        layout = (RelativeLayout) findViewById(R.id.progressLayoutLogin);

        loginPresenterImpl= new LoginPresenterImpl(this);

        //Listener para el boton de registro
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(getString(R.string.tag_fisiopia), "Clicked login button");
                //Se llama al presenter donde se realiza toda la lógica de negocio
                loginPresenterImpl.login(txtUsername.getText().toString(), txtPass.getText().toString());

            }
        });

        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(getString(R.string.tag_fisiopia), "Clicked register button");

                Intent intent = new Intent(LoginActivity.this,
                        RegisterActivity.class);
                startActivity(intent);
            }
        });

        Log.i(getString(R.string.tag_fisiopia), "Login View- END");

    }

    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void visibilityProgressBar(int visibility){

        Log.d(getString(R.string.tag_fisiopia), "Visibility for the login view");

        progressBarLogin.setVisibility(visibility);
        if(visibility == View.VISIBLE){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.VISIBLE);
        }else{
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            layout.setVisibility(View.GONE);
        }

    }

}
