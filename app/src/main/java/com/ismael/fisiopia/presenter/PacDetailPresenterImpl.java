package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;
import android.util.Log;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.PacienteService;
import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.async.UpdatePaciente;
import com.ismael.fisiopia.service.model.ChangePassword;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.PacienteDetailActivity;

/**
 * Created by Ismael on 12/10/2017.
 */

public class PacDetailPresenterImpl implements PacDetailPresenter {


    private PacienteDetailActivity mainActivity;
    private PacienteService pacienteService;
    private UsuarioService usuarioService;
    private SessionManager sessionManager;

    public PacDetailPresenterImpl(PacienteDetailActivity mainActivity) {

        this.mainActivity = mainActivity;
        pacienteService = new PacienteService(mainActivity);
        usuarioService = new UsuarioService(mainActivity);
        sessionManager = new SessionManager(mainActivity);
    }


    public void obtenerDatosPaciente(String username) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Obtain information of the patient - START");


        //Obtenemos el token del usuario de la app
        String token = sessionManager.getToken();

        //obtenemos los datos del profesional
        pacienteService.obtenerPaciente(username, token);


        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Obtain information of the patient - END");
    }

    @Override
    public void mostrarDatosPaciente() {
        Paciente paciente = sessionManager.getPacienteDetails();
        mainActivity.mostrarDatosPaciente(paciente.getUser().getUsername(), paciente.getUser().getFirstName(), paciente.getUser().getLastName(),
                paciente.getUser().getEmail(), paciente.getDireccion(), paciente.getDni());
    }

    @Override
    public void logout() {
        // Clearing all data from Shared Preferences
        sessionManager.logoutUser();
        mainActivity.logout();
    }

    public boolean modificarDatosPaciente(String username, String nombre, String apellidos, String email, String direccion, String dni){
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Modify patient information - START");
        boolean result;

        if(!username.isEmpty() && !nombre.isEmpty() && !apellidos.isEmpty() && !email.isEmpty() && !direccion.isEmpty()
                && !dni.isEmpty()) {

            Paciente paciente = new Paciente();
            User usuario = new User();

            String pass = sessionManager.getPass();
            usuario.setUsername(username);
            usuario.setFirstName(nombre);
            usuario.setLastName(apellidos);
            usuario.setEmail(email);
            usuario.setPassword(pass);

            paciente.setUser(usuario);
            paciente.setDireccion(direccion);
            paciente.setDni(dni);

            new UpdatePaciente(paciente, mainActivity.getApplicationContext() , this).execute();

            result = true;

        }else{
            this.mostrarMensaje("Debe rellenar todos los datos");

            result = false;
        }


        Log.i(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Patient information had been modified");
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Modify patient information - END");

        return result;

    }

    public void changePass(DialogInterface dialog, String oldPass, String newPass){
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Change pass of the user - START");

        String token = sessionManager.getToken();
        Paciente paciente = sessionManager.getPacienteDetails();

        ChangePassword changePassword = new ChangePassword();
        changePassword.setUsername(paciente.getUser().getUsername());
        changePassword.setPassword(oldPass);
        changePassword.setNewPassword(newPass);

        usuarioService.changePass(dialog, token, changePassword );

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Change pass of the user - END");


    }

    public void visibilityProgressBar(int visibility){
        mainActivity.visibilityProgressBar(visibility);
    }


    public void cambiarIconoDetail(int icon){
        mainActivity.setimageBotonEditar(icon);
    }

    public void disabledEditTextDetail(){
        mainActivity.disabledEditTextDetail();
    }

    public void mostrarMensaje(String mensaje){
        mainActivity.mostrarMensaje(mensaje);
    }

    @Override
    public void deletePaciente() {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Delete a Patient - START");

        String token = sessionManager.getToken();
        String username = sessionManager.getUsername();
        pacienteService.deletePaciente(username,token);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacDetailPresenter] Delete a Patient - END");

    }


}
