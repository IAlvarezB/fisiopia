package com.ismael.fisiopia.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.android.gms.games.internal.player.ProfileSettingsEntity;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.views.LoginActivity;

import java.util.HashMap;

/**
 * Created by ismael.alvarez on 21-Nov-17.
 */

/*
    Clase que controla los sharedPreferences
 */
public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "com.ismael.fisiopia";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    // Username del usuario
    public static final String KEY_ESPACIENTE = "esPaciente";
    // Username del usuario
    public static final String KEY_USERNAME = "username";
    //Nombre del usuario
    public static final String KEY_NAME = "nombre";
    //Apellidos del usuario
    public static final String KEY_APELLIDOS = "apellidos";
    // Email del usuario
    public static final String KEY_EMAIL = "email";
    // Direccion del usuario
    public static final String KEY_DIRECCION = "direccion";
    // Direccion del usuario
    public static final String KEY_NUMCOLEGIADO = "numColegiado";
    //Id del perfil de paciente
    public static final String KEY_ID_PACIENTE = "pacienteId";
    //Id del perfil de profesional
    public static final String KEY_ID_PROFESIONAL = "profesionalId";
    //Token asignado al usuario para poder realizar las peticiones correctamente
    public static final String KEY_TOKEN = "token";
    public static final String KEY_DNI = "dni";
    public static final String KEY_PASSWORD = "password";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session para profesional
     * */
    public void createLoginProfesionalSession(Profesional profesional){

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        //Se almacena para saber que perfil es
        editor.putBoolean(KEY_ESPACIENTE, false);
        editor.putString(KEY_USERNAME, profesional.getUser().getUsername());
        editor.putString(KEY_NAME, profesional.getUser().getFirstName());
        editor.putString(KEY_APELLIDOS, profesional.getUser().getLastName());
        editor.putString(KEY_EMAIL, profesional.getUser().getEmail());
        editor.putInt(KEY_ID_PROFESIONAL, profesional.getId());
        editor.putString(KEY_NUMCOLEGIADO, profesional.getNumColegiado());
        editor.putString(KEY_DNI, profesional.getDni());
        editor.putString(KEY_DIRECCION, profesional.getDireccion());
        editor.putString(KEY_PASSWORD, profesional.getUser().getPassword());

        // commit changes
        editor.commit();
    }

    /**
     * Create login session para paciente
     * */
    public void createLoginPacienteSession(Paciente paciente){

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        //Se almacena para saber que perfil es
        editor.putBoolean(KEY_ESPACIENTE, true);
        editor.putString(KEY_USERNAME, paciente.getUser().getUsername());
        editor.putString(KEY_NAME, paciente.getUser().getFirstName());
        editor.putString(KEY_APELLIDOS, paciente.getUser().getLastName());
        editor.putString(KEY_EMAIL, paciente.getUser().getEmail());
        editor.putInt(KEY_ID_PACIENTE, paciente.getId());
        editor.putString(KEY_DNI, paciente.getDni());
        editor.putString(KEY_DIRECCION, paciente.getDireccion());
        editor.putString(KEY_PASSWORD, paciente.getUser().getPassword());
        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }



    /**
     * Get stored session data
     * */
    public Profesional getProfesionalDetails(){
        Profesional profesional = new Profesional();
        User user = new User();


        // user name
        user.setUsername(pref.getString(KEY_USERNAME, null));
        user.setFirstName(pref.getString(KEY_NAME, null));
        user.setLastName(pref.getString(KEY_APELLIDOS, null));
        user.setEmail(pref.getString(KEY_EMAIL, null));
        user.setPassword(pref.getString(KEY_PASSWORD, null));
        profesional.setUser(user);

        profesional.setDireccion(pref.getString(KEY_DIRECCION, null));
        profesional.setDni(pref.getString(KEY_DNI, null));
        profesional.setId(pref.getInt(KEY_ID_PROFESIONAL, -1));
        profesional.setNumColegiado(pref.getString(KEY_NUMCOLEGIADO, null));

        // return profesional
        return profesional;
    }

    /**
     * Get stored session data
     * */
    public Paciente getPacienteDetails(){
        Paciente paciente = new Paciente();
        User user = new User();


        // user name
        user.setUsername(pref.getString(KEY_USERNAME, null));
        user.setFirstName(pref.getString(KEY_NAME, null));
        user.setLastName(pref.getString(KEY_APELLIDOS, null));
        user.setEmail(pref.getString(KEY_EMAIL, null));
        user.setPassword(pref.getString(KEY_PASSWORD, null));
        paciente.setUser(user);

        paciente.setDireccion(pref.getString(KEY_DIRECCION, null));
        paciente.setDni(pref.getString(KEY_DNI, null));
        paciente.setId(pref.getInt(KEY_ID_PROFESIONAL, -1));

        // return paciente
        return paciente;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }


    // Get token for the user
    public String getToken(){
        return pref.getString(KEY_TOKEN, null);
    }

    // Set token for the user
    public void setToken(String token){
        editor.putString(KEY_TOKEN, token);
        // commit changes
        editor.commit();
    }

    public String getPass(){
        return pref.getString(KEY_PASSWORD, null);
    }

    public String getUsername(){
        return pref.getString(KEY_USERNAME,null);
    }
    public void putUsername(String username){
        editor.putString(KEY_USERNAME, username);

        editor.commit();
    }

    public boolean getEsPaciente(){
        return pref.getBoolean(KEY_ESPACIENTE, true);
    }

}
