package com.ismael.fisiopia.presenter;

import android.util.Log;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.PacienteService;
import com.ismael.fisiopia.service.ProfesionalService;
import com.ismael.fisiopia.service.async.CargarHorarios;
import com.ismael.fisiopia.service.async.CreateHorario;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.util.HorarioArrayAdapter;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.PacienteMainActivity;
import com.ismael.fisiopia.views.ProfesionalMainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ismael on 12/10/2017.
 */

public class ProfMainPresenterImpl implements ProfMainPresenter {


    private ProfesionalMainActivity mainActivity;
    private ProfesionalService profesionalService;
    private SessionManager sessionManager;

    public ProfMainPresenterImpl(ProfesionalMainActivity mainActivity) {

        this.mainActivity = mainActivity;
        profesionalService = new ProfesionalService(mainActivity);
        sessionManager = new SessionManager(mainActivity);
    }


    public void obtenerDatosProfesional(String username) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Obtain information of the profesional - START");

        //Obtenemos el token del usuario de la app
        String token = sessionManager.getToken();

        //obtenemos los datos del profesional
        profesionalService.obtenerProfesional(username, token);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Obtain information of the profesional - END");

    }


    @Override
    public void logout() {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Logout - START");
        // Clearing all data from Shared Preferences
        sessionManager.logoutUser();
        mainActivity.logout();

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Logout - END");
    }

    @Override
    public void createHorario(String horaini, String horafin, List<String> dias) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Create schedule - START");
        List<Horario> horarios = new ArrayList<Horario>();
        String username = sessionManager.getUsername();

        for (String item : dias) {
            Horario horario = new Horario();

            horario.setDia(item);
            horario.setHoraini(horaini);
            horario.setHorafin(horafin);
            horario.setProfesional(username);
            horarios.add(horario);
        }

        new CreateHorario(mainActivity.getApplicationContext(), this, horarios).execute();

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Create schedule - END");

    }

    @Override
    public void cargarHorariosProfesional() {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Load schedule - START");
        String username = sessionManager.getUsername();
        new CargarHorarios(mainActivity.getApplicationContext(), this, username).execute();

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Load schedule - END");
    }

    public void visibilityProgressBar(int visibility) {
        mainActivity.visibilityProgressBar(visibility);
    }

    @Override
    public void mostrarHorarios(List<Horario> horarios) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Show schedule - START");
        List<Horario[]> arrayHorarios = new ArrayList<>();

        if (!horarios.isEmpty()) {
            HashMap<Integer, List<Horario>> horariosMap = this.dividirHorarios(horarios);
            for (int i = 0; i < horariosMap.size(); i++) {

                List<Horario> horarioList = horariosMap.get(i);
                arrayHorarios.add(horarioList.toArray(new Horario[0]));

            }

        }
        mainActivity.mostrarHorarios(arrayHorarios);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Show schedule - END");
    }

    @Override
    public HashMap<Integer, List<Horario>> dividirHorarios(List<Horario> horarios) {

        HashMap<Integer, List<Horario>> map = new HashMap<>();
        List<Horario> horariosAux = new ArrayList<>();
        int keyMap = 0;
        String horaIni = new String();
        String horaFin = new String();

        for (int i = 0; i < horarios.size(); i++) {

            if (!horaIni.equals(horarios.get(i).getHoraini()) && !horaFin.equals(horarios.get(i).getHorafin())) {
                horaIni = horarios.get(i).getHoraini();
                horaFin = horarios.get(i).getHorafin();
                horariosAux.add(horarios.get(i));

                for (int j = 0; j < horarios.size(); j++) {
                    if (j != i && horarios.get(j).getHoraini().equals(horaIni) && horarios.get(j).getHorafin().equals(horaFin)) {
                        horariosAux.add(horarios.get(j));
                    }
                }

                map.put(keyMap, horariosAux);
                keyMap++;
                horariosAux = new ArrayList<>();
            }
        }

        return map;
    }

    @Override
    public void deleteHorario(String horaIni, String horaFin, List<String> dias) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Delete schedule - START");

        String token = sessionManager.getToken();
        String username = sessionManager.getUsername();

        for (String dia : dias) {
            Horario horarioDelete = new Horario();
            horarioDelete.setHoraini(horaIni);
            horarioDelete.setHorafin(horaFin);
            horarioDelete.setProfesional(username);
            horarioDelete.setDia(dia);
            profesionalService.deleteHorario(token, horarioDelete);
        }

        this.cargarHorariosProfesional();

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfMainPresenter] Delete schedule - END");

    }

}
