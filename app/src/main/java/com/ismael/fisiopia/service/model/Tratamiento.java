package com.ismael.fisiopia.service.model;

/**
 * Created by Ismael on 17/11/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tratamiento {

    @SerializedName("profesional")
    @Expose
    private String profesional;
    @SerializedName("paciente")
    @Expose
    private String paciente;
    @SerializedName("titTratamiento")
    @Expose
    private String titTratamiento;
    @SerializedName("desTratamiento")
    @Expose
    private String desTratamiento;

    public String getProfesional() {
        return profesional;
    }

    public void setProfesional(String profesional) {
        this.profesional = profesional;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getTitTratamiento() {
        return titTratamiento;
    }

    public void setTitTratamiento(String titTratamiento) {
        this.titTratamiento = titTratamiento;
    }

    public String getDesTratamiento() {
        return desTratamiento;
    }

    public void setDesTratamiento(String desTratamiento) {
        this.desTratamiento = desTratamiento;
    }

    @Override
    public String toString() {
        return "Tratamiento{" +
                "profesional=" + profesional +
                ", paciente=" + paciente +
                ", titTratamiento='" + titTratamiento + '\'' +
                ", desTratamiento='" + desTratamiento + '\'' +
                '}';
    }
}
