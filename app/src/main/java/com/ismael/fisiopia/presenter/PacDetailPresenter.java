package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface PacDetailPresenter {

    void mostrarDatosPaciente();

    boolean modificarDatosPaciente(String username, String nombre, String apellidos, String email, String direccion, String dni);

    void logout();

    void changePass(DialogInterface dialog, String oldPass, String newPass);

    void visibilityProgressBar(int visibility);

    void cambiarIconoDetail(int icon);

    void disabledEditTextDetail();

    void mostrarMensaje(String mensaje);

    void deletePaciente();

}