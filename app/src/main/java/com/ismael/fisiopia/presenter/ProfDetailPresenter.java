package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface ProfDetailPresenter {

    void logout();

    boolean modificarDatosProfesional(String username, String nombre, String apellidos, String email, String direccion, String numColegiado, String dni);

    void mostrarDatosProfesional();

    void changePass(DialogInterface dialog, String oldPass, String newPass);

    void deleteProfesional();

}