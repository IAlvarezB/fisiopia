package com.ismael.fisiopia.presenter;

import com.ismael.fisiopia.dto.UserDto;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface LoginPresenter {

    void login(String username, String password);

}