package com.ismael.fisiopia.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;

/**
 * Created by ismaelalvarez on 1/01/18.
 */

public class MyArrayAdapter extends ArrayAdapter<Tratamiento> {
    private final Context context;
    private final Tratamiento[] tratamientos;

    public MyArrayAdapter(Context context, Tratamiento[] tratamientos) {
        super(context, R.layout.rowlayout, tratamientos);
        this.context = context;
        this.tratamientos = tratamientos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        TextView txtUsername = (TextView) rowView.findViewById(R.id.tratUsername);
        TextView txtNombreCompleto = (TextView) rowView.findViewById(R.id.tratNombre);
        EditText txtObservaciones = (EditText) rowView.findViewById(R.id.tratObservaciones);

        Tratamiento item = tratamientos[position];

        txtUsername.setText(item.getPaciente());
        txtNombreCompleto.setText(item.getTitTratamiento());
        txtObservaciones.setText(item.getDesTratamiento());

        return rowView;
    }
}
