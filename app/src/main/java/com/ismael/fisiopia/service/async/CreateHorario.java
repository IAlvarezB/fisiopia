package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacMainPresenter;
import com.ismael.fisiopia.presenter.PacMainPresenterImpl;
import com.ismael.fisiopia.presenter.ProfMainPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class CreateHorario extends AsyncTask<Void, Integer, List<Horario>> {

    private SessionManager sessionManager;
    private ProfMainPresenterImpl profMainPresenter;
    private Context context;
    private List<Horario> horarios;

    public CreateHorario(Context context, ProfMainPresenterImpl profMainPresenterImpl, List<Horario> horarios) {

        this.sessionManager = new SessionManager(context);
        this.profMainPresenter = profMainPresenterImpl;
        this.context = context;
        this.horarios= horarios;
    }


    @Override
    protected void onPreExecute() {

        Log.d(context.getString(R.string.tag_fisiopia), "[CreateHorario] Create schedule service - START");

        profMainPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected List<Horario> doInBackground(Void... voids) {

        List<Horario> result = new ArrayList<Horario>();

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());

        for(Horario item : horarios){
            Call<Horario> call = mAPIService.createHorario(item);

            try {
                Horario horario = call.execute().body();
                if (horario != null) {
                    result.add(item);
                } else {
                    result = null;
                }

            } catch (IOException e) {
                e.printStackTrace();
                result = null;
                Log.e(context.getString(R.string.tag_fisiopia), "[CreateHorario] Create schedule sercvice ERROR!", e);
            }
        }

        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        profMainPresenter.visibilityProgressBar(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(List<Horario> result) {
        if(result != null || !result.isEmpty() ){
            Toast.makeText(context,"Horario creado correctamente", Toast.LENGTH_LONG);
        }else{
            Toast.makeText(context,"Error al crear el horario, vuelva a intentarlo más tarde", Toast.LENGTH_LONG);
        }
        profMainPresenter.visibilityProgressBar(View.GONE);

        Log.d(context.getString(R.string.tag_fisiopia), "[CreateHorario] Create schedule service - END");
    }

}
