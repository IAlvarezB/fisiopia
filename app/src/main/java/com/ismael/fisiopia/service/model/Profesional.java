package com.ismael.fisiopia.service.model;

/**
 * Created by Ismael on 17/11/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profesional {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("dni")
    @Expose
    private String dni;
    @SerializedName("numColegiado")
    @Expose
    private String numColegiado;
    @SerializedName("valoracionMedia")
    @Expose
    private Integer valoracionMedia;
    @SerializedName("numVotos")
    @Expose
    private Integer numVotos;
    @SerializedName("direccion")
    @Expose
    private String direccion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNumColegiado() {
        return numColegiado;
    }

    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }

    public Integer getValoracionMedia() {
        return valoracionMedia;
    }

    public void setValoracionMedia(Integer valoracionMedia) {
        this.valoracionMedia = valoracionMedia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getNumVotos() {
        return numVotos;
    }

    public void setNumVotos(Integer numVotos) {
        this.numVotos = numVotos;
    }

    @Override
    public String toString() {
        return "Profesional{" +
                "id=" + id +
                ", user=" + user +
                ", dni='" + dni + '\'' +
                ", numColegiado='" + numColegiado + '\'' +
                ", valoracionMedia=" + valoracionMedia +
                ", numVotos=" + numVotos +
                '}';
    }
}
