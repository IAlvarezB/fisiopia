package com.ismael.fisiopia.service;

/**
 * Created by Ismael on 17/11/2017.
 */

import com.ismael.fisiopia.service.model.ChangePassword;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.service.model.User;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @POST("/pacientes/")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<Paciente> savePaciente(@Body Paciente paciente);

    @POST("/profesionales/")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<Profesional> saveProfesional(@Body Profesional profesional);

    @GET("/profesionales/")
    Call<List<Profesional>> getProfesionales();

    @GET("/pacientesUsername/")
    Call<List<User>> getPacientesUsername();

    @POST("/login/")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<User> login(@Body User user);

    @GET("/profesionales/{username}/")
    Call<Profesional> getProfesionalByUsername(@Path("username") String username);

    @GET("/pacientes/{username}/")
    Call<Paciente> getPacienteByUsername(@Path("username") String username);

    @DELETE("/profesionales/{username}/")
    Call<Void> deleteProfesional(@Path("username") String username);

    @DELETE("/pacientes/{username}/")
    Call<Void> deletePaciente(@Path("username") String username);

    @GET("/tratamientos/{username}/")
    Call<List<Tratamiento>> getTratamientoByUsername(@Path("username") String username);

    @PUT("/pacientes/")
    Call<Paciente> updatePaciente(@Body Paciente paciente);

    @PUT("/profesionales/")
    Call<Profesional> updateProfesional(@Body Profesional profesional);

    @PUT("/changepass/")
    Call<ChangePassword> changePass(@Body ChangePassword changepass);

    @HTTP(method = "DELETE", path = "/tratamientos/", hasBody = true)
    Call<Tratamiento> deleteTratamiento(@Body Tratamiento tratamiento);

    @POST("/tratamientos/")
    Call<Tratamiento> createTratamiento(@Body Tratamiento tratamiento);

    @POST("/horarios/")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<Horario> createHorario(@Body Horario horario);

    @GET("/horarios/{username}")
    Call<List<Horario>> getHorarioByUsername(@Path("username") String username);

    @HTTP(method = "DELETE", path = "/horarios/", hasBody = true)
    Call<Void> deleteHorario(@Body Horario horario);
}
