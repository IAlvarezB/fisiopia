package com.ismael.fisiopia.service;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.TratamientoPresenterImpl;
import com.ismael.fisiopia.service.async.GetTratamientos;
import com.ismael.fisiopia.service.model.ChangePassword;
import com.ismael.fisiopia.service.model.PlacesResults;
import com.ismael.fisiopia.service.model.Prediction;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.PacienteMainActivity;
import com.ismael.fisiopia.views.ProfesionalMainActivity;
import com.ismael.fisiopia.views.TratamientosActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ismael on 17/11/2017.
 */

public class UsuarioService {

    private APIService mAPIService;
    private GoogleAPIService gAPIService;
    SessionManager sessionManager;
    Context appContext;

    private static final String API_KEY = "AIzaSyBwkeIBOghcVGrokY4ynk6shs7_KLwnkl8";

    public UsuarioService(Context context) {
        super();
        appContext = context.getApplicationContext();
        sessionManager = new SessionManager(context);
    }

    public void loginUser(final User user) {

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Login process - START");

        mAPIService = ApiUtils.getAPIService(null);
        mAPIService.login(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                Intent main = new Intent();

                if (response.isSuccessful()) {

                    Log.i(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Login success");

                    //Exito
                    if (response.body().getToken() != null && !response.body().getToken().isEmpty()) {

                        //Comprobamos que tipo de perfil es. Paciente o profesional
                        if (response.body().getEsPaciente()) {
                            sessionManager.setToken(response.body().getToken());
                            main = new Intent(appContext, PacienteMainActivity.class);
                            main.putExtra("username", user.getUsername());
                        } else {
                            sessionManager.setToken(response.body().getToken());
                            main = new Intent(appContext, ProfesionalMainActivity.class);
                            main.putExtra("username", user.getUsername());
                        }

                        appContext.startActivity(main);

                    } else {
                        Toast.makeText(appContext, "Error inesperado", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(appContext, "Credenciales incorrectas, inténtelo de nuevo", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Login process", t);
                //Error
            }
        });

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Login process - END");

    }

    //Elementos de autocompletado del registro.
    //Lo hacemos sincrono
    public ArrayList<String> autocompleteRegister(String types, String input, String components) {

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Autocomplete register process - START");


        ArrayList<String> result = new ArrayList<String>();
        try {
            gAPIService = ApiUtils.getGoogleAPIService();
            Call<PlacesResults> call = gAPIService.getAddressResults(types, input, components, API_KEY);

            PlacesResults placesResults = call.execute().body();
            for (Prediction item : placesResults.getPredictions()) {
                result.add(item.getDescription());
            }
        } catch (IOException e) {
            Log.e("[Fisiopia]", e.getMessage());
        }


        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Autocomplete register process - END");
        return result;
    }

    public void changePass(final DialogInterface dialog, String token, ChangePassword changePass) {

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Change password process - START");

        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.changePass(changePass).enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {

                if (response.isSuccessful()) {
                    dialog.cancel();
                    Toast.makeText(appContext, "Contraseña cambiada correctamente", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(appContext, "Error al cambiar la contraseña. Vuelva a intentarlo.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                Log.e(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Change password process ERROR!", t);
                Toast.makeText(appContext, "Error al cambiar la contraseña. Vuelva a intentarlo.", Toast.LENGTH_LONG).show();
                //Error
            }
        });

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Change password process - END");
    }

    public void getUsernames(final TratamientosActivity activity, String token, final AutoCompleteTextView usernameTratamiento) {
        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Get usernames process - START");

        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.getPacientesUsername().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> listUsers = response.body();
                List<String> usernames = new ArrayList<String>();

                for (User user : listUsers) {
                    usernames.add(user.getUsername());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(appContext,
                        android.R.layout.simple_dropdown_item_1line, usernames);

                usernameTratamiento.setAdapter(adapter);


            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Get usernames process ERROR!", t);
            }
        });

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Get usernames process - END");
    }


    public void createTratamiento(final DialogInterface dialog, String token, Tratamiento tratamiento, final TratamientoPresenterImpl tratamientoPresenter) {
        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Create a new treatment process - START");

        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.createTratamiento(tratamiento).enqueue(new Callback<Tratamiento>() {
            @Override
            public void onResponse(Call<Tratamiento> call, Response<Tratamiento> response) {

                String username = sessionManager.getUsername();

                //new GetTratamientos(username, appContext, tratamientoPresenter);
                tratamientoPresenter.obtenerTratamientos();
                dialog.cancel();

                Toast.makeText(appContext, "Tratamiento añadido correctamente", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Tratamiento> call, Throwable t) {
                Log.e(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Create a new treatment process ERROR!", t);
                Toast.makeText(appContext, "Error al intentar crear un nuevo tratamiento, íntentelo mas tarde", Toast.LENGTH_LONG).show();

            }
        });

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Create a new treatment process - END");
    }

    public void deleteTratamiento(String token, Tratamiento tratamiento, final TratamientoPresenterImpl tratamientoPresenter) {
        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Delete a treatment process - START");

        mAPIService = ApiUtils.getAPIService(token);
        mAPIService.deleteTratamiento(tratamiento).enqueue(new Callback<Tratamiento>() {
            @Override
            public void onResponse(Call<Tratamiento> call, Response<Tratamiento> response) {

                //new GetTratamientos(username, appContext, tratamientoPresenter);
                tratamientoPresenter.obtenerTratamientos();

                Toast.makeText(appContext, "Tratamiento eliminado correctamente", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Tratamiento> call, Throwable t) {
                Log.e(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Delete a treatment process ERROR!", t);
                Toast.makeText(appContext, "Error al intentar eliminar el tratamiento, íntentelo mas tarde", Toast.LENGTH_LONG).show();

            }
        });

        Log.d(appContext.getString(R.string.tag_fisiopia), "[UsuarioService] Delete a treatment process - END");
    }

}
