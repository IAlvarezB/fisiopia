package com.ismael.fisiopia.views;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.ismael.fisiopia.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CrearProfesionalTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Rule
    public GrantPermissionRule grantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION);


    private LoginActivity mActivity = null;

    private static final String TOAST_STRING_DATOS  = "Debe rellenar todos los datos para poder darse de alta en FISIOPIA";

    private static final String TOAST_STRING_EMAIL  = "Las direcciones de correo electrónico no coinciden";


    @Before
    public void setActivity() {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void crearProfesionalTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("estenombrenoesta111"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("estanombrenoesta1"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("estenombrenoesta"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatRadioButton = onView(
                allOf(withId(R.id.radioFisio), withText("Fisioterapeuta"),
                        childAtPosition(
                                allOf(withId(R.id.perfilGroup),
                                        childAtPosition(
                                                withId(R.id.register_activity),
                                                8)),
                                0)));
        appCompatRadioButton.perform(scrollTo(), click());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("12"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        Thread.sleep(3000L);

        ViewInteraction editText = onView(
                allOf(withId(R.id.txtUserNameLogin),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                1),
                        isDisplayed()));
        editText.check(matches(isDisplayed()));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.txtPassLogin),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                2),
                        isDisplayed()));
        editText2.check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un profesional si no se introduce un nombre de usuario
    @Test
    public void crearProfesionalSinUsernameTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }


    //Test que comprueba que no se puede crear un Profesional si no se introduce una password
    @Test
    public void crearProfesionalSinPassTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce un nombre de Profesional.
    @Test
    public void crearProfesionalSinNombreTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce los apellidos
    @Test
    public void crearProfesionalSinApellidosTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce un dni
    @Test
    public void crearProfesionalSinDniTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce un correo.
    @Test
    public void crearProfesionalSinCorreoTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce la confirmación del correo.
    @Test
    public void crearProfesionalSinCorreoConfirmaciónTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText(""), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());


        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce el perfil de Profesional.
    //@Test
    //TODO: WIP
    public void crearProfesionalSinPerfilTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());


        onView(withId(R.id.btnRegistro))
                .perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un Profesional si no se introduce una dirección.
    @Test
    public void crearProfesionalSinDireccionTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que no se puede crear un profesional si no se introduce un numero de colegiado
    @Test
    public void crearProfesionalSinNumeroDeColegiadoTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("Profesionaltest"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        onView(withId(R.id.radioFisio))
                .perform(click());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("30"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        onView(withText(this.TOAST_STRING_DATOS)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    //Test que comprueba que si se introducen dos correos diferentes falla.
    @Test
    public void crearProfesionalErrorConfirmacionEmailTest() throws InterruptedException {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.txtRegistroNuevo), withText("¿No tienes cuenta? - Registrate aquí"),
                        childAtPosition(
                                allOf(withId(R.id.activity_main),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                3),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txtUserName),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText("estenombrenoesta111"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtPassword),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                1)));
        appCompatEditText2.perform(scrollTo(), replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txtNombre),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                2)));
        appCompatEditText3.perform(scrollTo(), replaceText("estanombrenoesta1"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txtApellidos),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                3)));
        appCompatEditText4.perform(scrollTo(), replaceText("estenombrenoesta"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txtDni),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                4)));
        appCompatEditText5.perform(scrollTo(), replaceText("76234567R"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txtEmail), withContentDescription("Correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                5)));
        appCompatEditText6.perform(scrollTo(), replaceText("test@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txtEmailConfirm), withContentDescription("Confirmación del correo electrónico"),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                6)));
        appCompatEditText7.perform(scrollTo(), replaceText("emailincorrectconfirm@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatRadioButton = onView(
                allOf(withId(R.id.radioFisio), withText("Fisioterapeuta"),
                        childAtPosition(
                                allOf(withId(R.id.perfilGroup),
                                        childAtPosition(
                                                withId(R.id.register_activity),
                                                8)),
                                0)));
        appCompatRadioButton.perform(scrollTo(), click());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txtNumColegiado),
                        childAtPosition(
                                allOf(withId(R.id.register_activity),
                                        childAtPosition(
                                                withClassName(is("android.widget.ScrollView")),
                                                0)),
                                9)));
        appCompatEditText8.perform(scrollTo(), replaceText("12345"), closeSoftKeyboard());

        onView(withId(R.id.txtDireccion))
                .perform(scrollTo())
                .perform(typeText("calle"));
        Thread.sleep(3000L);

        onView(withText("Calle de Alcalá, Madrid, Spain"))
                .inRoot(isPlatformPopup())
                .perform(click());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txtNumeroDireccion),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.layoutDireccion),
                                        0),
                                1)));
        appCompatEditText9.perform(scrollTo(), replaceText("12"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnRegistro), withText("Quiero Fisiopia!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());


        onView(withText(this.TOAST_STRING_EMAIL)).inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView())))).check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
