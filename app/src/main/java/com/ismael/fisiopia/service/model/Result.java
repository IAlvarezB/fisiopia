
package com.ismael.fisiopia.service.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    /**
     * The Formatted_address Schema 
     * <p>
     * 
     * 
     */
    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    /**
     * The Place_id Schema 
     * <p>
     * 
     * 
     */

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
