package com.ismael.fisiopia.presenter;

import com.google.android.gms.maps.GoogleMap;
import com.ismael.fisiopia.dto.UserDto;
import com.ismael.fisiopia.util.GooglePlacesAutocompleteAdapter;

/**
 * Created by Ismael on 12/10/2017.
 */

public interface RegisterPresenter {

    void crearUsuario(UserDto user, String txtEmailConfirm);
}