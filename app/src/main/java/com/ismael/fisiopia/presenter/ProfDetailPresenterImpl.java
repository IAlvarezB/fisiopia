package com.ismael.fisiopia.presenter;

import android.content.DialogInterface;
import android.util.Log;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.ProfesionalService;
import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.async.UpdateProfesional;
import com.ismael.fisiopia.service.model.ChangePassword;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.User;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.ProfesionalDetailActivity;

/**
 * Created by Ismael on 12/10/2017.
 */

public class ProfDetailPresenterImpl implements ProfDetailPresenter {


    private ProfesionalDetailActivity mainActivity;
    private ProfesionalService profesionalService;
    private UsuarioService usuarioService;
    private SessionManager sessionManager;

    public ProfDetailPresenterImpl(ProfesionalDetailActivity mainActivity) {

        this.mainActivity = mainActivity;
        profesionalService = new ProfesionalService(mainActivity);
        sessionManager = new SessionManager(mainActivity);
        usuarioService = new UsuarioService(mainActivity);

    }


    @Override
    public void logout() {
        // Clearing all data from Shared Preferences
        sessionManager.logoutUser();
        mainActivity.logout();
    }

    @Override
    public void mostrarDatosProfesional() {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Obtain information of the profesional - START");

        Profesional profesional = sessionManager.getProfesionalDetails();
        mainActivity.mostrarDatosProfesional(profesional.getUser().getUsername(), profesional.getUser().getFirstName(), profesional.getUser().getLastName(),
                profesional.getUser().getEmail(), profesional.getDireccion(), profesional.getNumColegiado(), profesional.getDni());

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Obtain information of the profesional - END");
    }

    public boolean modificarDatosProfesional(String username, String nombre, String apellidos, String email, String direccion, String numColegiado, String dni) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Modify information of the profesional - START");
        boolean result;

        Profesional profesional = new Profesional();
        User usuario = new User();

        if (!username.isEmpty() && !nombre.isEmpty() && !apellidos.isEmpty() && !email.isEmpty() && !direccion.isEmpty()
                && !numColegiado.isEmpty() && !dni.isEmpty()) {
            String pass = sessionManager.getPass();
            usuario.setUsername(username);
            usuario.setFirstName(nombre);
            usuario.setLastName(apellidos);
            usuario.setEmail(email);
            usuario.setPassword(pass);

            profesional.setUser(usuario);
            profesional.setDireccion(direccion);
            profesional.setNumColegiado(numColegiado);
            profesional.setDni(dni);

            new UpdateProfesional(profesional, mainActivity.getApplicationContext(), this).execute();

            result = true;
        } else {
            this.mostrarMensaje("Debe rellenar todos los datos");

            result = false;
        }
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Modify information of the profesional - END");

        return result;
    }

    public void changePass(DialogInterface dialog, String oldPass, String newPass) {
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Change pass of the profesional - START");

        String token = sessionManager.getToken();
        Paciente paciente = sessionManager.getPacienteDetails();

        ChangePassword changePassword = new ChangePassword();
        changePassword.setUsername(paciente.getUser().getUsername());
        changePassword.setPassword(oldPass);
        changePassword.setNewPassword(newPass);

        usuarioService.changePass(dialog, token, changePassword);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Change pass of the profesional - END");
    }

    public void visibilityProgressBar(int visibility) {
        mainActivity.visibilityProgressBar(visibility);
    }


    public void cambiarIconoDetail(int icon) {
        mainActivity.setimageBotonEditar(icon);
    }

    public void disabledEditTextDetail() {
        mainActivity.disabledEditTextDetail();
    }

    public void mostrarMensaje(String mensaje) {
        mainActivity.mostrarMensaje(mensaje);
    }

    @Override
    public void deleteProfesional() {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Delete profesional - START");

        String token = sessionManager.getToken();
        String username = sessionManager.getUsername();
        profesionalService.deleteProfesional(username, token);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[ProfDetailPresenter] Delete profesional - END");

    }


}
