package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacMainPresenter;
import com.ismael.fisiopia.presenter.PacMainPresenterImpl;
import com.ismael.fisiopia.presenter.ProfDetailPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class GetAllProfesionales extends AsyncTask<Void, Integer, List<Profesional>> {

    private SessionManager sessionManager;
    private PacMainPresenter pacienteMainPresenter;
    private Context context;
    private UsuarioService usuarioService;

    public GetAllProfesionales(Context context, PacMainPresenterImpl pacienteMainPresenterImpl) {

        this.sessionManager = new SessionManager(context);
        this.pacienteMainPresenter = pacienteMainPresenterImpl;
        this.context = context;
        this.usuarioService = new UsuarioService(context);
    }


    @Override
    protected void onPreExecute() {

        Log.d(context.getString(R.string.tag_fisiopia), "[GetAllProfesionales] Get all profesionals service - START");


        pacienteMainPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected List<Profesional> doInBackground(Void... voids) {

        List<Profesional> result;

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());
        Call<List<Profesional>> call = mAPIService.getProfesionales();

        try {
            List<Profesional> profesionales = call.execute().body();
            if (profesionales != null && !profesionales.isEmpty()) {


                result = profesionales;
            } else {
                result = null;

            }

        } catch (IOException e) {
            e.printStackTrace();
            result = null;
            Log.e(context.getString(R.string.tag_fisiopia), "[GetAllProfesionales] Get all profesionals service ERROR!", e);

        }

        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        pacienteMainPresenter.visibilityProgressBar(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(List<Profesional> result) {

        pacienteMainPresenter.visibilityProgressBar(View.GONE);
            pacienteMainPresenter.mostrarMarkers(result);

        Log.d(context.getString(R.string.tag_fisiopia), "[GetAllProfesionales] Get all profesionals service - END");
    }

}
