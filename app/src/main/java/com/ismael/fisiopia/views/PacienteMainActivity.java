package com.ismael.fisiopia.views;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacMainPresenterImpl;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.util.DrawerUtil;
import com.ismael.fisiopia.util.HorarioArrayAdapter;

import java.util.List;

public class PacienteMainActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, LocationListener, OnMapReadyCallback {

    private PacMainPresenterImpl pacMainPresenterImpl;
    private LocationManager locationManager;
    private SupportMapFragment map;
    private String provider;
    static final Integer REQUEST_LOCATION = 0x1;
    private ProgressBar progressBar;
    private GoogleMap googleMap;
    private TextView emptyHorario;
    private ListView horarioView;
    private static String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(getString(R.string.tag_fisiopia), "[PatientView] Patient view - START ");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pacmain_activity);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMainPac);
        pacMainPresenterImpl = new PacMainPresenterImpl(this);
        map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.pacientemap);
        setTitle(R.string.title_app);

        if (savedInstanceState == null) {
            map.getMapAsync(this);

        }
        //Barra de app
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle("Encuentra a tu Profesional");


        if (getIntent().getExtras() != null) {
            username = getIntent().getExtras().getString("username");
            pacMainPresenterImpl.obtenerDatosPaciente(username);
        }

        DrawerUtil.getDrawer(this, myToolbar, username);

        Log.d(getString(R.string.tag_fisiopia), "[PatientView] Patient view - END ");

    }


    public void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void logout() {
        Log.d(getString(R.string.tag_fisiopia), "[PatientView] Logout - START");

        // After logout redirect user to Loing Activity
        Intent i = new Intent(PacienteMainActivity.this, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        startActivity(i);

        Log.d(getString(R.string.tag_fisiopia), "[PatientView] Logout - END");

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("[FISIOPIA]", " [PatientView] Enabled new location provider" + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("[FISIOPIA]", "[PatientView] Disabled location provider" + provider);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.i("[FISIOPIA]", "[PatientView] Map ready ");

        pacMainPresenterImpl.obtenerProfesionales();
        googleMap.setOnMarkerClickListener(this);

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.googleMap = googleMap;
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("[FISIOPIA]", "Error permisos localización");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);

        } else {

            googleMap.setMyLocationEnabled(true);
            Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17);
            googleMap.animateCamera(cameraUpdate);
        }


    }

    public void setMarker(LatLng place, Profesional profesional) {

        Log.d("[FISIOPIA]", "[PatientView] Set marker in: " + place.toString());

        Marker marker = googleMap.addMarker(new MarkerOptions().position(place)
                .title(profesional.getDireccion()));
        marker.setTag(profesional);

    }

    public void visibilityProgressBar(int visibility) {
        progressBar.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

    }


    @Override
    public boolean onMarkerClick(Marker marker) {


        Profesional profMarker = (Profesional) marker.getTag();

        Log.i("[FISIOPIA]", "[PatientView] Click marker: " + profMarker.getUser().getUsername());

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        final LayoutInflater inflater = this.getLayoutInflater();

        View content = inflater.inflate(R.layout.dialog_profcontact, null);
        builder.setView(content)
                // Add action buttons
                .setNeutralButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        //Assign id to Tabhost.
        TabHost tabHostWindow = (TabHost) (content).findViewById(android.R.id.tabhost);
        tabHostWindow.setup();
        //Creating tab menu.
        TabHost.TabSpec tabContact = tabHostWindow.newTabSpec("Datos personales");
        TabHost.TabSpec tabSchedule = tabHostWindow.newTabSpec("Horario");

        //Setting up tab 1 name.
        tabContact.setIndicator("Datos personales");
        tabContact.setContent(R.id.contactLayout);

        //Setting up tab 2 name.
        tabSchedule.setIndicator("Horario");
        tabSchedule.setContent(R.id.scheduleLayout);

        //Adding tab1, tab2 to tabhost view.
        tabHostWindow.addTab(tabContact);
        tabHostWindow.addTab(tabSchedule);


        final TextView txtUsernameProfesional = (TextView) (content).findViewById(R.id.txtProfUsername);
        final TextView txtNumColegiadoProfesional = (TextView) (content).findViewById(R.id.txtProfNumColegiado);
        final TextView txtEmailProfesional = (TextView) (content).findViewById(R.id.txtProfEmail);
        final TextView txtDireccionProfesional = (TextView) (content).findViewById(R.id.txtProfAddress);

        txtDireccionProfesional.setElegantTextHeight(true);
        txtDireccionProfesional.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        txtDireccionProfesional.setSingleLine(false);

        txtUsernameProfesional.setText(profMarker.getUser().getUsername());
        txtNumColegiadoProfesional.setText(profMarker.getNumColegiado());
        txtEmailProfesional.setText(profMarker.getUser().getEmail());
        txtDireccionProfesional.setText(profMarker.getDireccion());


        emptyHorario = (TextView) (content).findViewById(R.id.txtEmptyHorario);
        horarioView = (ListView) content.findViewById(R.id.horarioview);

        pacMainPresenterImpl.cargarHorariosProfesional(profMarker.getUser().getUsername());

        builder.show();


        return true;
    }

    public void mostrarHorarios(List<Horario[]> arrayHorarios) {

        Log.i("[FISIOPIA]", "[PatientView] Show schedules");

        if (arrayHorarios.isEmpty()) {
            emptyHorario.setVisibility(View.VISIBLE);
        } else {
            emptyHorario.setVisibility(View.GONE);
        }

        HorarioArrayAdapter adapter = new HorarioArrayAdapter(this, arrayHorarios);
        horarioView.setAdapter(adapter);

    }
}
