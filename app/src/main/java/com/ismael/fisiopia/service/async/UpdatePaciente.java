package com.ismael.fisiopia.service.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacDetailPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Paciente;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class UpdatePaciente extends AsyncTask <Void, Integer, Boolean>{

    private Paciente paciente;
    private SessionManager sessionManager;
    private PacDetailPresenterImpl pacDetailPresenter;
    private Context context;


    public UpdatePaciente(Paciente paciente, Context context, PacDetailPresenterImpl pacienteDetailActivity) {
        this.paciente = paciente;
        this.sessionManager = new SessionManager(context);
        this.pacDetailPresenter = pacienteDetailActivity;
        this.context = context;
    }


    @Override
    protected void onPreExecute()
    {
        Log.d(context.getString(R.string.tag_fisiopia), "[UpdatePaciente] Update patient service - START");
        pacDetailPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        boolean result= false;

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());
        Call<Paciente> call = mAPIService.updatePaciente(paciente);

        try {
            Paciente paciente = call.execute().body();
            if(paciente != null){
                sessionManager.createLoginPacienteSession(paciente);
                result = true;
            }else{
                result = false;

            }

        } catch (IOException e) {
            e.printStackTrace();
            result = false;
            Log.e(context.getString(R.string.tag_fisiopia), "[UpdatePaciente] Update patient service ERROR!", e);

        }

        return result;
    }
    @Override
    protected void onProgressUpdate(Integer... values)
    {
        pacDetailPresenter.visibilityProgressBar(View.VISIBLE);
    }
    @Override
    protected void onPostExecute(Boolean result)
    {
        pacDetailPresenter.visibilityProgressBar(View.GONE);

        if(!result){
            pacDetailPresenter.mostrarMensaje("Error al actualizar los datos. Inténtelo más tarde");
        }else{
            pacDetailPresenter.cambiarIconoDetail(R.mipmap.ic_edit_white_24dp);
            pacDetailPresenter.mostrarMensaje("Datos acutalizados correctamente");
            pacDetailPresenter.disabledEditTextDetail();
        }

        Log.d(context.getString(R.string.tag_fisiopia), "[UpdatePaciente] Update patient service - END");

    }

}
