package com.ismael.fisiopia.service.async;

import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;

import com.ismael.fisiopia.R;
import com.ismael.fisiopia.presenter.PacMainPresenter;
import com.ismael.fisiopia.presenter.PacMainPresenterImpl;
import com.ismael.fisiopia.presenter.TratamientoPresenter;
import com.ismael.fisiopia.presenter.TratamientoPresenterImpl;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.model.Tratamiento;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.SessionManager;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ismaelalvarez on 18/12/17.
 */

public class GetTratamientos extends AsyncTask<Void, Integer, List<Tratamiento>> {

    private SessionManager sessionManager;
    private TratamientoPresenter tratamientoPresenter;
    private Context context;
    private String username;

    public GetTratamientos(String username, Context context, TratamientoPresenterImpl tratamientoPresenterImpl) {

        this.sessionManager = new SessionManager(context);
        this.tratamientoPresenter = tratamientoPresenterImpl;
        this.context = context;
        this.username=username;
    }


    @Override
    protected void onPreExecute() {
        Log.d(context.getString(R.string.tag_fisiopia), "[GetTratamientos] Treatments service - START");

        tratamientoPresenter.visibilityProgressBar(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected List<Tratamiento> doInBackground(Void... voids) {

        List<Tratamiento> result;

        APIService mAPIService = ApiUtils.getAPIService(sessionManager.getToken());
        Call<List<Tratamiento>> call = mAPIService.getTratamientoByUsername(username);

        try {
            List<Tratamiento> tratamientos = call.execute().body();
            if (tratamientos != null && !tratamientos.isEmpty()) {


                result = tratamientos;
            } else {
                result = null;

            }

        } catch (IOException e) {
            e.printStackTrace();
            result = null;
            Log.e(context.getString(R.string.tag_fisiopia), "[GetTratamientos] Treatments service ERROR!", e);

        }

        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        tratamientoPresenter.visibilityProgressBar(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(List<Tratamiento> result) {
        tratamientoPresenter.mostrarTratamientos(result);
        tratamientoPresenter.visibilityProgressBar(View.GONE);


        Log.d(context.getString(R.string.tag_fisiopia), "[GetTratamientos] Treatments service - END");
    }

}
