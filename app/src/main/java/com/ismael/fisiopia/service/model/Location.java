
package com.ismael.fisiopia.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    /**
     * The Lat Schema 
     * <p>
     * 
     * 
     */
    @SerializedName("lat")
    @Expose
    private double lat = 0.0D;
    /**
     * The Lng Schema 
     * <p>
     * 
     * 
     */
    @SerializedName("lng")
    @Expose
    private double lng = 0.0D;

    /**
     * The Lat Schema 
     * <p>
     * 
     * 
     */
    public double getLat() {
        return lat;
    }

    /**
     * The Lat Schema 
     * <p>
     * 
     * 
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * The Lng Schema 
     * <p>
     * 
     * 
     */
    public double getLng() {
        return lng;
    }

    /**
     * The Lng Schema 
     * <p>
     * 
     * 
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

}
