package com.ismael.fisiopia.presenter;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.ismael.fisiopia.R;
import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.GoogleAPIService;
import com.ismael.fisiopia.service.PacienteService;
import com.ismael.fisiopia.service.UsuarioService;
import com.ismael.fisiopia.service.async.CargarHorariosParaPacientes;
import com.ismael.fisiopia.service.async.GetAllProfesionales;
import com.ismael.fisiopia.service.model.Horario;
import com.ismael.fisiopia.service.model.LocationResult;
import com.ismael.fisiopia.service.model.Profesional;
import com.ismael.fisiopia.service.remote.ApiUtils;
import com.ismael.fisiopia.util.GooglePlacesAutocompleteAdapter;
import com.ismael.fisiopia.util.SessionManager;
import com.ismael.fisiopia.views.PacienteMainActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ismael on 12/10/2017.
 */

public class PacMainPresenterImpl implements PacMainPresenter {


    private PacienteMainActivity mainActivity;
    private PacienteService pacienteService;
    private SessionManager sessionManager;
    private GoogleAPIService gAPIService;
    private static final String API_KEY = "AIzaSyCq6hK3vgIpUkxuFTWPIi5MsJlIb1RiIuY";

    public PacMainPresenterImpl(PacienteMainActivity mainActivity) {

        this.mainActivity = mainActivity;
        pacienteService = new PacienteService(mainActivity);
        sessionManager = new SessionManager(mainActivity);


    }


    public void obtenerDatosPaciente(String username) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Obtain information of the patient - START");

        //Obtenemos el token del usuario de la app
        String token = sessionManager.getToken();

        //obtenemos los datos del profesional
        pacienteService.obtenerPaciente(username, token);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Obtain information of the patient - END");

    }

    @Override
    public void logout() {
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Logout- START");

        // Clearing all data from Shared Preferences
        sessionManager.logoutUser();
        mainActivity.logout();

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Logout- END");
    }

    public void obtenerProfesionales() {

        new GetAllProfesionales(mainActivity.getApplicationContext(), this).execute();
    }

    public void mostrarMarkers(List<Profesional> profesionales) {
        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Show marker into the map - START");
        try {
            Geocoder gc = new Geocoder(mainActivity);
            List<Address> list = null;
            List<LatLng> places = new ArrayList<LatLng>();
            if (gc.isPresent()) {
                for (Profesional item : profesionales) {
                    list = gc.getFromLocationName(item.getDireccion(), 1);
                    if (list.isEmpty()) {
                        Log.e("[FISIOPIA]", "Dirección de " + item.getUser().getUsername() + " no encontrada");
                    } else {
                        Address address = list.get(0);
                        mainActivity.setMarker(new LatLng(address.getLatitude(), address.getLongitude()), item);
                    }
                }
            }

        } catch (IOException e) {

            for (final Profesional item : profesionales) {

                if (item.getDireccion() != null && !item.getDireccion().isEmpty()) {
                    gAPIService = ApiUtils.getGoogleAPIService();
                    Call<LocationResult> call = gAPIService.getLocationByAddress(item.getDireccion(), API_KEY);
//

                    call.enqueue(new Callback<LocationResult>() {
                        @Override
                        public void onResponse(Call<LocationResult> call, Response<LocationResult> response) {
                            //Do something with response
                            try {

                                LocationResult googleMapResponse = response.body();

                                LatLng localizacion = new LatLng(googleMapResponse.getResults().get(0).getGeometry().getLocation().getLat(),
                                        googleMapResponse.getResults().get(0).getGeometry().getLocation().getLng());

                                mainActivity.setMarker(localizacion, item);

                            } catch (Exception ex) {
                                Log.e("[FISIOPIA]", "Error en el servicio para obtener lacalización " + ex);
                            }
                        }

                        @Override
                        public void onFailure(Call<LocationResult> call, Throwable t) {
                            //Do something with failure
                        }
                    });

                }
            }
        }


        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Show marker into the map - END");
    }

    public void visibilityProgressBar(int visibility) {
        mainActivity.visibilityProgressBar(visibility);
    }

    public void cargarHorariosProfesional(String usernameProfesional) {
        new CargarHorariosParaPacientes(mainActivity.getApplicationContext(), this, usernameProfesional).execute();
    }

    @Override
    public void mostrarHorarios(List<Horario> horarios) {

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Show schedules - START");

        HashMap<Integer, List<Horario>> horariosMap = this.dividirHorarios(horarios);
        List<Horario[]> arrayHorarios = new ArrayList<>();

        for (int i = 0; i < horariosMap.size(); i++) {

            List<Horario> horarioList = horariosMap.get(i);
            arrayHorarios.add(horarioList.toArray(new Horario[0]));

        }

        mainActivity.mostrarHorarios(arrayHorarios);

        Log.d(mainActivity.getString(R.string.tag_fisiopia), "[PacPresenter] Show schedules - END");
    }

    @Override
    public HashMap<Integer, List<Horario>> dividirHorarios(List<Horario> horarios) {
        HashMap<Integer, List<Horario>> map = new HashMap<>();
        List<Horario> horariosAux = new ArrayList<>();
        int keyMap = 0;
        String horaIni = new String();
        String horaFin = new String();

        for (int i = 0; i < horarios.size(); i++) {

            if (!horaIni.equals(horarios.get(i).getHoraini()) && !horaFin.equals(horarios.get(i).getHorafin())) {
                horaIni = horarios.get(i).getHoraini();
                horaFin = horarios.get(i).getHorafin();
                horariosAux.add(horarios.get(i));

                for (int j = 0; j < horarios.size(); j++) {
                    if (j != i && horarios.get(j).getHoraini().equals(horaIni) && horarios.get(j).getHorafin().equals(horaFin)) {
                        horariosAux.add(horarios.get(j));
                    }
                }

                map.put(keyMap, horariosAux);
                keyMap++;
                horariosAux = new ArrayList<>();
            }
        }

        return map;
    }

}
