package com.ismael.fisiopia.service.remote;

import com.ismael.fisiopia.service.APIService;
import com.ismael.fisiopia.service.GoogleAPIService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Ismael on 17/11/2017.
 */

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "https://fisiopiapi.herokuapp.com/";
    public static final String BASE_GOOGLEAPI_URL = "https://maps.googleapis.com";

    public static APIService getAPIService(String token) {

        return RetrofitClient.getClient(BASE_URL, token).create(APIService.class);
    }

    public static GoogleAPIService getGoogleAPIService() {

        return RetrofitClient.getClient(BASE_GOOGLEAPI_URL, null).create(GoogleAPIService.class);
    }
}
